package TesterClass;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * This class stores methods that filters, sorts, and lists down:
 * a. Most and least danceable tacks
 * b. Most and least instrumental tracks
 * c. Most and least speechiness tracks
 * d. High and low energetic tracks
 * e. Tracks in major and minor keys
 * f. Loudest and soft-volume tracks
 * g. Most and least acoustic tracks
 * h. Most and least lively tracks
 * i. Tracks with high and low valence
 * j. Tracks with high and low tempo
 */
public class JanDaleTester {
    static Path csvFile = Path.of("src", "res/New File.csv"); //initialized file path
    static String line = "";


    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        //  sortItem(scanner);
//getHighestAndLowestTempoSongs();
        //getMostAndLeastPopularSongs();
        // getQuietestAndLoudestSongs();
        // getMostAndLeastEnergeticSongs();
        //  sortItem();
        // searchItem("tiktok dance");
        //  getMostAndLeastSpeechinessSongs();
        // getOldestAndNewestSongs();
        sortItem(scanner);


        JanDaleTester JanDaleTester = new JanDaleTester();
        //  JanDaleTester.searchArtistName("doja cat");
        //JanDaleTester.searchTrackName("boss bitch");
        //  JanDaleTester.searchGenre("tiktok dance");
        // JanDaleTester


    }//end of main method


    /**
     * This method takes a CSV line as input, splits it into an array of fields using a comma as the delimiter, and then uses these fields to create and return an TesterClass.InformationRequirements object.
     *
     * @param line
     * @return
     */
    public static InformationRequirements getInfoReq(String line) {
        String[] fields;
        if (!line.contains("\"")) {
            fields = line.split(",");
        } else {
            fields = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1);
        }

        return new InformationRequirements(
                fields[0], fields[1], fields[2], fields[3], fields[4], fields[5], fields[6],
                fields[7], fields[8], fields[9], fields[10], fields[11], fields[12],
                fields[13], fields[14], fields[15], fields[16], fields[17], fields[18],
                fields[19], fields[20], fields[21], fields[22]
        );
    }


    /**
     * This method displays the danceability from LEAST TO MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingDanceability() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getDanceability))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Danceability: " + forMusic.getDanceability() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);


        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the danceability from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingDanceability() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getDanceability).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Danceability: " + forMusic.getDanceability() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
    }//end of method


    /**
     * This method displays the instrumentalness from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingInstrumentalness() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getInstrumentalness).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Instrumentalness: " + forMusic.getInstrumentalness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the instrumentalness from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void ascendingInstrumentalness() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getInstrumentalness))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Instrumentalness: " + forMusic.getInstrumentalness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);


        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the SPEECHINESS from LEAST TO MOST
     *
     * @throws IOException
     */
    public static void ascendingSpeechiness() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getSpeechiness))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Speechiness: " + forMusic.getSpeechiness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the SPEECHINESS from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingSpeechiness() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getSpeechiness).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Speechiness: " + forMusic.getSpeechiness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);


        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the ENERGY from LEAST TO MOST
     *
     * @throws IOException
     */
    public static void ascendingEnergy() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getEnergy))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Energy: " + forMusic.getEnergy() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the ENERGY from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingEnergy() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getEnergy).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Energy: " + forMusic.getEnergy() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the tracks in MAJOR KEY (1)
     *
     * @throws IOException
     */
    public static void majorKeyTrack() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .filter(informationRequirements -> "1".equalsIgnoreCase(informationRequirements.getKey()))//searches the objects
                .sorted(Comparator.comparing(InformationRequirements::getTrackName).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Key: " + forMusic.getKey() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the tracks in MINOR KEY (0)
     *
     * @throws IOException
     */
    public static void minorKeyTrack() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .filter(informationRequirements -> "0".equalsIgnoreCase(informationRequirements.getKey()))//searches the objects
                .sorted(Comparator.comparing(InformationRequirements::getTrackName).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Key: " + forMusic.getKey() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the tracks and their VOLUME from LOUDEST TO LOWER SOUND VOLUMES
     *
     * @throws IOException
     */
    public static void descendingLoudness() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getLoudness).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Loudness: " + forMusic.getLoudness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the tracks and their VOLUME from LOWER SOUND VOLUMES TO LOUDEST
     *
     * @throws IOException
     */
    public static void ascendingLoudness() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getLoudness))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Loudness: " + forMusic.getLoudness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    } // end of method


    /**
     * This method displays the ACOUSTIC from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingAcoustic() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getAcousticness).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Acousticness: " + forMusic.getAcousticness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the ACOUSTICNESS from LEAST TO MOST
     *
     * @throws IOException
     */
    public static void ascendingAcoustic() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getAcousticness))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Acousticness: " + forMusic.getAcousticness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays LIVELINESS from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingLively() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getLiveness).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Liveness: " + forMusic.getLiveness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the LIVELINESS from LEAST to MOST
     *
     * @throws IOException
     */
    public static void ascendingLively() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getLiveness))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Liveness: " + forMusic.getLiveness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the tracks and their VALENCE from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingValence() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getValence).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Valence: " + forMusic.getValence() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the tracks and their LIVELINESS from LEAST to MOST
     *
     * @throws IOException
     */
    public static void ascendingValence() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getLiveness))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Valence: " + forMusic.getValence() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the tracks and their TEMPO from LEAST to MOST
     *
     * @throws IOException
     */
    public static void ascendingTempo() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getTempo))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Tempo: " + forMusic.getTempo() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the tracks and their TEMPO from MOST to LEAST
     *
     * @throws IOException
     */
    public static void descendingTempo() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getTempo).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Tempo: " + forMusic.getTempo() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the track ID from LEAST TO MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingTrackID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getTrackID))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Track ID: " + forMusic.getTrackID() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the track ID from MOST TO LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingTrackID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getTrackID).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Track ID: " + forMusic.getTrackID() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the track name from LEAST TO MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingTrackName() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getTrackName))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the track name from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingTrackName() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getTrackName).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the artist ID from LEAST TO MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingArtistID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getArtistID))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Arist ID: " + forMusic.getArtistID() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the artist ID from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingArtistID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getArtistID).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Arist ID: " + forMusic.getArtistID() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the artist name from LEAST TO MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingArtistName() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getArtistName))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Artist Name: " + forMusic.getArtistName() + " || Artist ID: " + forMusic.getArtistID() + " || Track name: " + forMusic.getTrackName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the artist name from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingArtistName() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getArtistName).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Artist Name: " + forMusic.getArtistName() + " || Artist ID: " + forMusic.getArtistID() + " || Track name: " + forMusic.getTrackName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the artist name from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingAlbumID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getAlbumID))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Album ID: " + forMusic.getArtistName() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the artist name from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingAlbumID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getAlbumID).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Album ID: " + forMusic.getArtistName() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the duration from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingDuration() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getDurationMilliSecs))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Duration: " + forMusic.getDurationMilliSecs() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the duration from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingDuration() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getDurationMilliSecs).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Duration: " + forMusic.getDurationMilliSecs() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the release Date from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingReleaseDate() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getReleaseDate))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Release Date: " + forMusic.getReleaseDate() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the release Date from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingReleaseDate() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getReleaseDate).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Release Date: " + forMusic.getReleaseDate() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the popularity from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingPopularity() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getPopularity))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Popularity: " + forMusic.getPopularity() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the popularity from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingPopularity() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getPopularity).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Popularity: " + forMusic.getPopularity() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the playlist ID from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingPlaylistID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getPlaylistID))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Playlist ID: " + forMusic.getPlaylistID() + " || Playlist Name: " + forMusic.getPlaylistName() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the playlist ID from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingPlaylistID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getPlaylistID).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Playlist ID: " + forMusic.getPlaylistID() + " || Playlist Name: " + forMusic.getPlaylistName() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the playlist ID from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingPlaylistName() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getPlaylistName))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Playlist Name: " + forMusic.getPlaylistName() + " || Playlist ID: " + forMusic.getPlaylistID() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the playlist ID from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingPlaylistName() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getPlaylistName).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Playlist Name: " + forMusic.getPlaylistName() + " || Playlist ID: " + forMusic.getPlaylistID();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the duration (minutes) from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingDurationMins() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getDurationMins))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Duration (Minutes): " + forMusic.getDurationMins() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the duration (minutes) from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingDurationMins() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getDurationMins).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Duration (Minutes): " + forMusic.getDurationMins() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the genre from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingGenre() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getGenre))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Genre: " + forMusic.getGenre() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method

    /**
     * This method displays the genre from MOST to LEST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingGenre() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getGenre).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Genre: " + forMusic.getGenre() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    //METHODS USED FOR SEARCHING//

    /**
     * This method reads the lines of the file as a stream, and searches the streams to include only the objects where the TRACK NAME is found
     *
     * @param findItem
     * @throws IOException
     */
    public static void searchTrackName(String findItem) throws IOException {
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .filter(informationRequirements -> findItem.equalsIgnoreCase(informationRequirements.getTrackName()))//searches the objects
                .forEach(forMusic -> System.out.println(forMusic.getTrackName() + " || Artist: " + forMusic.getArtistName() + " || Album ID: " + forMusic.getAlbumID() + " || Genre: " + forMusic.getGenre()));//displays the filtered and sorted fields


    }//end of method


    /**
     * This method reads the lines of the file as a stream, and searches the streams to include only the objects where the ARTIST NAME is found
     *
     * @param findItem
     * @throws IOException
     */
    public static void searchArtistName(String findItem) throws IOException {
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .filter(informationRequirements -> findItem.equalsIgnoreCase(informationRequirements.getArtistName())) //searches the objects
                .forEach(forMusic -> System.out.println(forMusic.getArtistName() + " || Track: " + forMusic.getTrackName() + " || Album ID" + forMusic.getAlbumID() + " || Genre: " + forMusic.getGenre()));//displays the filtered and sorted fields
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and searches the streams to include only the objects where the PLAYLIST NAME is found
     *
     * @param findItem
     * @throws IOException
     */
    public static void searchPlaylistName(String findItem) throws IOException {
        Files.lines(csvFile)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .filter(informationRequirements -> findItem.equalsIgnoreCase(informationRequirements.getPlaylistName()))//searches the objects
                .forEach(forMusic -> System.out.println(forMusic.getPlaylistName() + " || Playlist ID: " + forMusic.getPlaylistID()));//displays the filtered and sorted fields
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and searches the streams to include only the objects where the GENRE is found
     *
     * @param findItem
     * @return
     * @throws IOException
     */
    public static void searchGenre(String findItem) throws IOException {
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .filter(informationRequirements -> findItem.equalsIgnoreCase(informationRequirements.getGenre()))//searches the objects
                .forEach(forMusic -> System.out.println(forMusic.getTrackName() + " || Artist: " + forMusic.getArtistName()));//displays the filtered and sorted fields

    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique genres with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterGenre() throws IOException {
        System.out.println("Category: Genre\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getGenre();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique track id's with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterTrackId() throws IOException {
        System.out.println("Category: Track ID\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getTrackID();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique track names with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterTrackName() throws IOException {
        System.out.println("Category: Track Name\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getTrackName();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique artist ID's with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterArtistID() throws IOException {
        System.out.println("Category: Artist ID\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getArtistID();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique track names with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterArtistName() throws IOException {
        System.out.println("Category: Artist Name\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getArtistName();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique album IDs with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterAlbumID() throws IOException {
        System.out.println("Category: Album ID\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getAlbumID();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique duration (milliseconds) with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterDuration() throws IOException {
        System.out.println("Category: Duration\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getDurationMilliSecs();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique release dates with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterReleaseDates() throws IOException {
        System.out.println("Category: Release Dates\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getReleaseDate();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique popularity rates with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterPopularity() throws IOException {
        System.out.println("Category: Popularity\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getPopularity();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique dancability with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterDanceability() throws IOException {
        System.out.println("Category: Danceability\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getDanceability();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique energies with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterEnergy() throws IOException {
        System.out.println("Category: Energy\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getEnergy();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique keys with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterKey() throws IOException {
        System.out.println("Category: Key\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items
        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getKey();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique loudness with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterLoudness() throws IOException {
        System.out.println("Category: Loudness\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items
        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getLoudness();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique modes with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterMode() throws IOException {
        System.out.println("Category: Mode\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items
        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getMode();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique spechiness with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterSpeechiness() throws IOException {
        System.out.println("Category: Speechiness\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getSpeechiness();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique acousticness with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterAcousticness() throws IOException {
        System.out.println("Category: Acousticness\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getAcousticness();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique loudness with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterInstrumentalness() throws IOException {
        System.out.println("Category: Instrumentalness\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getInstrumentalness();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique liveness with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterLiveness() throws IOException {
        System.out.println("Category: Liveness\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getLiveness();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique valence with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterValence() throws IOException {
        System.out.println("Category: Loudness\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getValence();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique tempos with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterTempo() throws IOException {
        System.out.println("Category: Tempo\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getTempo();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique playlist IDs with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterPlaylistIDs() throws IOException {
        System.out.println("Category: Playlist ID\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getPlaylistID();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique Playlist names with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterPlaylistName() throws IOException {
        System.out.println("Category: Playlist Name\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getPlaylistName();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method reads the lines of the file as a stream, and displays the unique duration (minutes) with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterDurationMins() throws IOException {
        System.out.println("Category: Duration (Minutes)\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items


        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getDurationMins();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method

    /**
     * This method displays the oldest and newest songs based on release date.
     *
     * @throws IOException
     */
    public static void getOldestAndNewestSongs() throws IOException {
        System.out.println("OLDEST AND NEWEST SONGS\n");
        // Find the oldest song
        System.out.println("Oldest Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getReleaseDate))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Release Date: " + oldestSong.getReleaseDate()));

        // Find the newest song
        System.out.println("\nNewest Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getReleaseDate))
                .ifPresent(newestSong -> System.out.println("Track: " + newestSong.getTrackName() +
                        " || Release Date: " + newestSong.getReleaseDate()));
    }//end of method

    /**
     * This method displays the quietest and loudest songs based on loudness.
     *
     * @throws IOException
     */
    public static void getQuietestAndLoudestSongs() throws IOException {
        System.out.println("QUIETEST AND LOUDEST SONGS\n");
        // Find the oldest song
        System.out.println("Quietest Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getLoudness))
                .ifPresent(quietestSong -> System.out.println("Track: " + quietestSong.getTrackName() +
                        " || Loudness: " + quietestSong.getLoudness()));

        // Find the newest song
        System.out.println("\nLoudest Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getLoudness))
                .ifPresent(loudestSong -> System.out.println("Track: " + loudestSong.getTrackName() +
                        " || Loudness: " + loudestSong.getLoudness()));
    }//end of method

    /**
     * This method displays the shortest and longest songs based on duration.
     *
     * @throws IOException
     */
    public static void getShortestAndLongestSongs() throws IOException {
        System.out.println("SHORTEST AND LONGEST SONGS\n");
        // Find the oldest song
        System.out.println("Shortest Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getDurationMilliSecs))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Duration: " + oldestSong.getDurationMilliSecs() + " || Duration (Minutes): " + oldestSong.getDurationMins()));

        // Find the newest song
        System.out.println("\nLongest Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getDurationMilliSecs))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Duration: " + oldestSong.getDurationMilliSecs() + " || Duration (Minutes): " + oldestSong.getDurationMins()));
    }//end of method

    /**
     * This method displays the most and least popular songs based on popularity.
     *
     * @throws IOException
     */
    public static void getMostAndLeastPopularSongs() throws IOException {
        System.out.println("MOST AND LEAST POPULAR SONGS\n");
        // Find the oldest song
        System.out.println("Least Popular Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getPopularity))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Popularity: " + oldestSong.getPopularity()));

        // Find the newest song
        System.out.println("\nMost Popular Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getPopularity))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Popularity: " + oldestSong.getPopularity()));
    }//end of method

    /**
     * This method displays the most and least danceable songs based on danceability.
     *
     * @throws IOException
     */
    public static void getMostAndLeastDanceableSongs() throws IOException {
        System.out.println("MOST AND LEAST DANCEABLE SONGS\n");
        // Find the oldest song
        System.out.println("Least Danceable Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getDanceability))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Danceability: " + oldestSong.getDanceability()));

        // Find the newest song
        System.out.println("\nMost Danceable Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getDanceability))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Danceability: " + oldestSong.getDanceability()));
    }//end of method

    /**
     * This method displays the most and least energetic songs based on energy.
     *
     * @throws IOException
     */
    public static void getMostAndLeastEnergeticSongs() throws IOException {
        System.out.println("MOST AND LEAST ENERGETIC SONGS\n");
        // Find the oldest song
        System.out.println("Least Energetic Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getEnergy))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Energy: " + oldestSong.getEnergy()));

        // Find the newest song
        System.out.println("\nMost Energetic Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getEnergy))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Energy: " + oldestSong.getEnergy()));
    }//end of method

    /**
     * This method displays the most and least lively songs based on liveness.
     *
     * @throws IOException
     */
    public static void getMostAndLeastLivelySongs() throws IOException {
        System.out.println("MOST AND LEAST LIVELY SONGS\n");
        // Find the oldest song
        System.out.println("Least Lively Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getLiveness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Liveness: " + oldestSong.getLiveness()));

        // Find the newest song
        System.out.println("\nMost Lively Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getLiveness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Liveness: " + oldestSong.getLiveness()));
    }//end of method

    /**
     * This method displays the highest and lowest valence songs based on valence.
     *
     * @throws IOException
     */
    public static void getHighestAndLowestValenceSongs() throws IOException {
        System.out.println("HIGHEST AND LOWEST VALENCE SONGS\n");
        // Find the oldest song
        System.out.println("Lowest Valence Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getValence))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Valence: " + oldestSong.getValence()));

        // Find the newest song
        System.out.println("\nHighest Valence Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getValence))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Valence: " + oldestSong.getValence()));
    }//end of method

    /**
     * This method displays the highest and lowest tempo songs based on tempo.
     *
     * @throws IOException
     */
    public static void getHighestAndLowestTempoSongs() throws IOException {
        System.out.println("HIGHEST AND LOWEST TEMPO SONGS\n");
        // Find the oldest song
        System.out.println("Lowest Tempo Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getTempo))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Tempo: " + oldestSong.getTempo()));

        // Find the newest song
        System.out.println("\nHighest Tempo Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getTempo))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Tempo: " + oldestSong.getTempo()));
    }//end of method

    /**
     * This method displays the most and least instrumental songs based on instrumentalness.
     *
     * @throws IOException
     */
    public static void getMostAndLeastInstrumentalSongs() throws IOException {
        System.out.println("MOST AND LEAST INSTRUMENTAL SONGS\n");
        // Find the oldest song
        System.out.println("Least Instrumental Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getInstrumentalness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Instrumentalness: " + oldestSong.getInstrumentalness()));

        // Find the newest song
        System.out.println("\nMost Lively Song:");
        Files.lines(csvFile)
                .skip(1)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getInstrumentalness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Instrumentalness: " + oldestSong.getInstrumentalness()));
    }//end of method

    /**
     * This method displays the most and least acoustic songs based on acousticness.
     *
     * @throws IOException
     */
    public static void getMostAndLeastAcousticSongs() throws IOException {
        System.out.println("MOST AND LEAST ACOUSTIC SONGS\n");
        // Find the oldest song
        System.out.println("Least Acoustic Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getAcousticness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Acousticness: " + oldestSong.getAcousticness()));

        // Find the newest song
        System.out.println("\nMost Lively Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getAcousticness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Acousticness: " + oldestSong.getAcousticness()));
    }//end of method

    /**
     * This method displays the most and least speechy songs based on speechiness.
     *
     * @throws IOException
     */
    public static void getMostAndLeastSpeechinessSongs() throws IOException {
        System.out.println("MOST AND LEAST SPEECHINESS SONGS\n");
        // Find the oldest song
        System.out.println("Least Speechy Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getSpeechiness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Speechinesss: " + oldestSong.getSpeechiness()));

        // Find the newest song
        System.out.println("\nMost Lively Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getSpeechiness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Speechinesss: " + oldestSong.getSpeechiness()));
    }//end of method

    public static void searchItem(String findItem) throws IOException {
        System.out.println("Searching for: " + findItem + "\n");
        searchGenre(findItem);
        searchGenre(findItem);
        searchArtistName(findItem);
        searchPlaylistName(findItem);
        searchPlaylistName(findItem);
        searchTrackName(findItem);
    }//end of method

    public static void sortItem(Scanner scanner) throws IOException {
        int sortNumber = 0;
        int sortOrder = 0;
        do{
            System.out.println("Select what to filter: 1. Track ID\n" +
                    "2. Track name\n" +
                    "3. Artist ID\n" +
                    "4. Artist name\n" +
                    "5. Album ID\n" +
                    "6. Duration\n" +
                    "7. Release date\n" +
                    "8. Popularity\n" +
                    "9. Danceability\n" +
                    "10. Energy\n" +
                    "11. Key\n" +
                    "12. Loudness\n" +
                    "14. Speechiness\n" +
                    "15. Acousticness\n" +
                    "16. Instrumentalness\n" +
                    "17. Liveness\n" +
                    "18. Valence\n" +
                    "19. Tempo\n" +
                    "20. Playlist ID\n" +
                    "21. Playlist name\n" +
                    "22. Duration mins\n" +
                    "23. Genre\n");
            sortNumber = scanner.nextInt();
            if (sortNumber != 1 && sortNumber != 2 && sortNumber != 3 && sortNumber != 4 && sortNumber != 5 && sortNumber != 6
                    && sortNumber != 7 && sortNumber != 8 && sortNumber != 9 && sortNumber != 10&& sortNumber != 11&& sortNumber != 12
                    && sortNumber != 14&& sortNumber != 15&& sortNumber != 16&& sortNumber != 17&& sortNumber != 18
                    && sortNumber != 19&& sortNumber != 20&& sortNumber != 21&& sortNumber != 22&& sortNumber != 23)
                System.out.println("Enter a number between 1-23!");
        }while(sortNumber != 1 && sortNumber != 2 && sortNumber != 3 && sortNumber != 4 && sortNumber != 5 && sortNumber != 6
                && sortNumber != 7 && sortNumber != 8 && sortNumber != 9 && sortNumber != 10&& sortNumber != 11&& sortNumber != 12
                && sortNumber != 14&& sortNumber != 15&& sortNumber != 16&& sortNumber != 17&& sortNumber != 18
                && sortNumber != 19&& sortNumber != 20&& sortNumber != 21&& sortNumber != 22&& sortNumber != 23);

        switch (sortNumber) {
            case 1:
                System.out.println("TRACK NAME" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingTrackID();
                        break;
                    case 2:
                        descendingTrackID();
                        break;
                }
                break;
            case 2:
                System.out.println("TRACK NAME" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingArtistName();
                        break;
                    case 2:
                        descendingArtistName();
                        break;
                }
                break;
            case 3:
                System.out.println("ARTIST ID" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingArtistID();
                        break;
                    case 2:
                        descendingArtistID();
                        break;
                }
                break;
            case 4:
                System.out.println("ARTIST NAME" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingArtistName();
                        break;
                    case 2:
                        descendingArtistName();
                        break;
                }
                break;
            case 5:
                System.out.println("ALBUM ID" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingAlbumID();
                        break;
                    case 2:
                        descendingAlbumID();
                        break;
                }
                break;
            case 6:
                System.out.println("DURATION" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingDuration();
                        break;
                    case 2:
                        descendingDuration();
                        break;
                }
                break;
            case 7:
                System.out.println("RELEASE DATE" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingReleaseDate();
                        break;
                    case 2:
                        descendingReleaseDate();
                        break;
                }
                break;
            case 8:
                System.out.println("POPULARITY" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingPopularity();
                        break;
                    case 2:
                        descendingPopularity();
                        break;
                }
                break;
            case 9:
                System.out.println("DANCEABILITY" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingDanceability();
                        break;
                    case 2:
                        descendingDanceability();
                        break;
                }
                break;
            case 10:
                System.out.println("ENERGY" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingEnergy();
                        break;
                    case 2:
                        descendingEnergy();
                        break;
                }
                break;
            case 11:
                System.out.println("KEY" +
                        "\n1. Major Key\n" +
                        "2. Minor Key");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        majorKeyTrack();
                        break;
                    case 2:
                        minorKeyTrack();
                        break;
                }
                break;
            case 12:
                System.out.println("LOUDNESS" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingLoudness();
                        break;
                    case 2:
                        descendingLoudness();
                        break;
                }
                break;
            case 14:
                System.out.println("SPEECHINESS" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingSpeechiness();
                        break;
                    case 2:
                        descendingSpeechiness();
                        break;
                }
                break;
            case 15:
                System.out.println("ACOUSTICNESS" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingAcoustic();
                        break;
                    case 2:
                        descendingAcoustic();
                        break;
                }
                break;
            case 16:
                System.out.println("ALBUM ID" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingInstrumentalness();
                        break;
                    case 2:
                        descendingInstrumentalness();
                        break;
                }
                break;
            case 17:
                System.out.println("LIVENESS" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingLively();
                        break;
                    case 2:
                        descendingLively();
                        break;
                }
                break;
            case 18:
                System.out.println("VALENCE" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingValence();
                        break;
                    case 2:
                        descendingValence();
                        break;
                }
                break;
            case 19:
                System.out.println("TEMPO" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingTempo();
                        break;
                    case 2:
                        descendingTempo();
                        break;
                }
                break;
            case 20:
                System.out.println("PLAYLIST ID" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingPlaylistID();
                        break;
                    case 2:
                        descendingPlaylistID();
                        break;
                }
                break;
            case 21:
                System.out.println("PLAYLIST NAME" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingPlaylistName();
                        break;
                    case 2:
                        descendingPlaylistName();
                        break;
                }
                break;
            case 22:
                System.out.println("DURATION MINUTES" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingDurationMins();
                        break;
                    case 2:
                        descendingDurationMins();
                        break;
                }
                break;
            case 23:
                System.out.println("GENRE" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingGenre();
                        break;
                    case 2:
                        descendingGenre();
                        break;
                }
                break;
        }//end of switch
    }//end of method


    public static void filterCategory(Scanner scanner) throws IOException {
        int filterNum = 0;
        do{
        System.out.println("Select what to filter: 1. Track ID\n" +
                "2. Track name\n" +
                "3. Artist ID\n" +
                "4. Artist name\n" +
                "5. Album ID\n" +
                "6. Duration\n" +
                "7. Release date\n" +
                "8. Popularity\n" +
                "9. Danceability\n" +
                "10. Energy\n" +
                "11. Key\n" +
                "12. Loudness\n" +
                "13. Mode\n" +
                "14. Speechiness\n" +
                "15. Acousticness\n" +
                "16. Instrumentalness\n" +
                "17. Liveness\n" +
                "18. Valence\n" +
                "19. Tempo\n" +
                "20. Playlist ID\n" +
                "21. Playlist name\n" +
                "22. Duration mins\n" +
                "23. Genre\n");
        filterNum = scanner.nextInt();
        if (filterNum != 1 && filterNum != 2 && filterNum != 3 && filterNum != 4 && filterNum != 5 && filterNum != 6
                && filterNum != 7 && filterNum != 8 && filterNum != 9 && filterNum != 10&& filterNum != 11&& filterNum != 12
                && filterNum != 13&& filterNum != 14&& filterNum != 15&& filterNum != 16&& filterNum != 17&& filterNum != 18
                && filterNum != 19&& filterNum != 20&& filterNum != 21&& filterNum != 22&& filterNum != 23)
            System.out.println("Enter a number between 1-23!");
    }while(filterNum != 1 && filterNum != 2 && filterNum != 3 && filterNum != 4 && filterNum != 5 && filterNum != 6
                && filterNum != 7 && filterNum != 8 && filterNum != 9 && filterNum != 10&& filterNum != 11&& filterNum != 12
                && filterNum != 13&& filterNum != 14&& filterNum != 15&& filterNum != 16&& filterNum != 17&& filterNum != 18
                && filterNum != 19&& filterNum != 20&& filterNum != 21&& filterNum != 22&& filterNum != 23);

        switch (filterNum) {
            case 1:
                System.out.println("TRACK ID\n");
                filterTrackId();
                break;
            case 2:
                System.out.println("TRACK NAME\n");
                filterTrackName();
                break;
            case 3:
                System.out.println("ARTIST ID\n");
                filterArtistID();
                break;
            case 4:
                System.out.println("ARTIST NAME\n");
                filterArtistName();
                break;
            case 5:
                System.out.println("ALBUM ID\n");
                filterAlbumID();
                break;
            case 6:
                System.out.println("DURATION\n");
                filterDuration();
                break;
            case 7:
                System.out.println("RELEASE DATE\n");
                filterReleaseDates();
                break;
            case 8:
                System.out.println("POPULARITY\n");
                filterPopularity();
                break;
            case 9:
                System.out.println("DANCEABILITY\n");
                filterDanceability();
                break;
            case 10:
                System.out.println("ENERGY\n");
                filterEnergy();
                break;
            case 11:
                System.out.println("KEY\n");
                filterKey();
                break;
            case 12:
                System.out.println("LOUDNESS\n");
                filterLoudness();
                break;
            case 14:
                System.out.println("SPEECHINESS\n");
                filterSpeechiness();
                break;
            case 15:
                System.out.println("ACOUSTICNESS\n");
                filterAcousticness();
                break;
            case 16:
                System.out.println("INSTRUMENTALNESS\n");
                filterInstrumentalness();
                break;
            case 17:
                System.out.println("LIVENESS\n");
                filterLiveness();
                break;
            case 18:
                System.out.println("VALENCE\n");
                filterValence();
                break;
            case 19:
                System.out.println("TEMPO\n");
                filterTempo();
                break;
            case 20:
                System.out.println("PLAYLIST ID\n");
                filterPlaylistIDs();
                break;
            case 21:
                System.out.println("PLAYLIST NAME\n");
                filterPlaylistName();
                break;
            case 22:
                System.out.println("DURATION MINS\n");
                filterDurationMins();
                break;
            case 23:
                System.out.println("GENRE\n");
                filterGenre();
                break;
        }//end of switch
    }//end of method
}//end of class
