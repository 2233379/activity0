package TesterClass;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class FelizTester {
    public static void main(String[] args) {
        menu();
    }

    public static void menu() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("\n==================================================");
        System.out.println("                       MENU                       ");
        System.out.println("==================================================");
        System.out.println(" 1. View songs by genre");
        System.out.println(" 2. Correlate song danceability with popularity");
        System.out.println(" 3. View the average loudness of songs by artists");
        System.out.println("--------------------------------------------------");
        System.out.print(" Input Choice: ");
        int choice = Integer.parseInt(scanner.nextLine());

        switch (choice) {
            case 1:
                filterByGenre();
                break;

            case 2:
                correlation();
                break;

            case 3:
                loudnessAnalysis();
                break;
        }
    }

    public static void filterByGenre() {
        String filePath = "src/res/9443 Dataset.csv";
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\n========================");
        System.out.println("     SONGS BY GENRE    ");
        System.out.println("========================");
        System.out.println(" 1. _TIKTOK");
        System.out.println(" 2. TIKTOK DANCE");
        System.out.println(" 3. TIKTOK OPM");
        System.out.println(" 4. TIKTOK PHILIPPINES");
        System.out.println("------------------------");
        System.out.print(" Input Choice: ");

        int choice = Integer.parseInt(scanner.nextLine());
        String genre = null;

        switch (choice) {
            case 1:
                genre = "_TIKTOK";
                break;

            case 2:
                genre = "TIKTOK DANCE";
                break;

            case 3:
                genre = "TIKTOK OPM";
                break;

            case 4:
                genre = "TIKTOK PHILIPPINES";
                break;
        }

        System.out.println("\n\n============================================================");
        System.out.println("                   SONGS UNDER " + genre);
        System.out.println("============================================================");

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            br.readLine();

            while ((line = br.readLine()) != null) {
                String[] columns = line.split(",");

                if (columns[22].equalsIgnoreCase(genre)) {
                    System.out.println(columns[1] + " by " + columns[3]);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * NOT FINAL
     */

    public static void correlation() {
        String filePath = "src/res/9443 Dataset.csv";
        double totalPopularity = 0.0;
        double totalDanceability = 0.0;
        double product = 0.0;
        double sqPopularity = 0.0;
        double sqDanceability = 0.0;
        int numberOfElements = 0;

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            br.readLine();  // Skip the first line

            while ((line = br.readLine()) != null) {
                numberOfElements++;

                String[] col;
                if (!line.contains("\"")) {
                    col = line.split(",");
                } else {
                    col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); // Ignoring commas inside quotation marks
                }

                try {
                    double popularity = Double.parseDouble(col[7]);
                    double danceability = Double.parseDouble(col[8]);

                    totalPopularity += popularity;
                    totalDanceability += danceability;
                    product += popularity * danceability;
                    sqPopularity += Math.pow(popularity, 2);
                    sqDanceability += Math.pow(danceability, 2);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        double correlation = ((numberOfElements * product) - (totalDanceability * totalPopularity)) /
                (Math.sqrt(((numberOfElements * sqDanceability) - Math.pow(totalDanceability, 2)) *
                        ((numberOfElements * sqPopularity) - Math.pow(totalPopularity, 2))));

        System.out.println("Total Popularity = " + totalPopularity);
        System.out.println("Total Danceability = " + totalDanceability);
        System.out.println("Popularity * Danceability = " + product);
        System.out.println("Popularity ^ 2 = " + sqPopularity);
        System.out.println("Danceability ^ 2 = " + sqDanceability);
        System.out.println("Number of Elements: " + numberOfElements);
        System.out.println("Correlation: " + correlation);

        if (correlation > 0.8) {
            System.out.println("Very Strong Positive");
            System.out.println("As danceability increases, popularity also significantly increases.");
        } else if (correlation > 0.6) {
            System.out.println("Strong Positive");
            System.out.println("There is a substantial positive correlation between popularity and danceability.");
        } else if (correlation > 0.4) {
            System.out.println("Moderate Positive");
            System.out.println("There is a moderate positive correlation between popularity and danceability.");
        } else if (correlation > 0.2) {
            System.out.println("Weak Positive");
            System.out.println("There is a weak positive correlation between popularity and danceability.");
        } else if (correlation >= -0.2 && correlation <= 0.2) {
            System.out.println("Weak or No Correlation");
            System.out.println(" Popularity and danceability are not significantly related.");
        } else if (correlation < -0.8) {
            System.out.println("Very Strong Negative");
            System.out.println("As danceability increases, popularity significantly decreases.");
        } else if (correlation < -0.6) {
            System.out.println("Strong Negative");
            System.out.println("There is a substantial negative correlation between popularity and danceability.");
        } else if (correlation < -0.4) {
            System.out.println("Moderate Negative");
            System.out.println("There is a moderate negative correlation between popularity and danceability.");
        } else {
            System.out.println("Weak Negative");
            System.out.println(" There is a weak negative correlation between popularity and danceability.");
        }
}

    public static void loudnessAnalysis() {
        Scanner scanner = new Scanner(System.in);
        String filePath = "src/res/9443 Dataset.csv";

        System.out.println("\n\n=====================================================================================================================================");
        System.out.println("                                                     AVERAGE LOUDNESS OF SONGS                                                       ");
        System.out.println("                                                 Please pick an artist of your choice                                                ");
        System.out.println("=====================================================================================================================================");
        String[] artists = {".swimwhere", "$NOT", "$uicideboy$", "$wave", "03 Greedo",
                "1096 Gang", "10k.Caash", "1mill Records", "1nonly", "1way Frank",
                "24hrs", "24kGoldn", "2CRE8", "300yearsyoung", "347aidan",
                "3irty", "3xbravo", "4LilPat3", "5 Seconds of Summer", "50 Cent",
                "6ix9ine", "7ru7h", "870glizzy", "88rising", "89ine",
                "A Boogie Wit da Hoodie", "A-FRAME", "A$AP Ferg", "A1 x J1", "A5H",
                "Aarika", "Aaron Smith", "Aaryan Shah", "Abaddon", "Absofacto",
                "ABSRDST", "Ace Poetik", "Adam Aronson", "Adam Bü", "Adam Kahati",
                "Addison Rae", "Adele", "Adso Alejandro", "Aegis", "Aesth",
                "Aests", "Aexcit", "Aieysha Haws", "Aikee", "Åírös",
                "AJ Gravity", "AJ Salvatore", "Ajay Stephens", "AJR", "Akano",
                "Akon", "Al James", "Al Moralde", "Alan Walker", "Aldo Bz",
                "Alessia Cara", "Alesso", "Alex Alexander", "Alex Aster", "Alex D'Rosso",
                "Alex Edmonds", "Alex Gonzaga", "Alex McArtor", "Alex Megane", "Alex Rose",
                "Alexa Ayaz", "Alexander 23", "Alexandra Stan", "Alfons", "Ali Gatie",
                "Alina Baraz", "All Time Low", "Alle Farben", "ALLMO$T", "Allocai",
                "Alok", "alt-J", "ALVIDO", "Alvin & The Chipmunks", "Aly & AJ",
                "Amaru Cloud", "Amaru Son", "Ambjaay", "American Authors", "Aminé",
                "Ana Shine", "Anamanaguchi", "Anderson .Paak", "Andre Swilley", "Andrew E.",
                "Andrew Gold", "Andrew Spencer", "Andy Grammer", "AnimeHub", "Anitta",
                "Anna Hamilton", "Anna Helen", "Anne-Marie", "Anoyd", "Anson Seabra",
                "Ant Saunders", "Anthem Hits Platinum", "Anthony Gonzalez", "Anthony Keyrouz", "Anthony Q.",
                "AP3", "Apekatten", "Apollo Fresh", "Apontay", "Aqyila",
                "Arctic Monkeys", "ARIA", "Aria X", "Ariana Grande", "Arizona Zervas",
                "Ark Patrol", "Armin van Buuren", "ARRIE", "Arts", "Arvey",
                "Ashe", "Ashley O", "Ashley Price", "Ashley Tha MC", "Ashnikko",
                "Asin", "Astrid S", "Astrokidjay", "Astrus*", "ATB",
                "Au/Ra", "Audrey Mika", "August Kid", "Auntie Hammy", "AURORA",
                "Austin Mahone", "Ava Max", "Avenue Beat", "Avi", "Avicii",
                "Avocuddle", "Avril Lavigne", "awfultune", "Axel Black & White", "Aya Nakamura",
                "AYO TR3", "Azealia Banks", "Azide", "Azjah", "B Free TMT",
                "B. Smyth", "B.o.B", "Baby Bash", "Baby Keem", "BabyJake",
                "Bad Bunny", "Badda TD", "Bag Raiders", "Bailey May", "Bandang Lapis",
                "Bankrol Hayden", "BANNERS", "Banx & Ranx", "Barrett Crake", "Basgilano",
                "Bassjackers", "Bay-C", "Bazzi", "bbno$", "BBY KODIE",
                "Bea Miller", "Beach Bunny", "Beach House", "Beachbag", "Beanz",
                "BeatKing", "Bebe Rexha", "Because", "Becky G", "Bee Gees",
                "bee$", "Behind The Frames", "Bell Biv DeVoe", "Bella Poarch", "Ben&Ben",
                "BENEE", "Benny Benassi", "benny blanco", "Benzi", "Besomorph",
                "Beyoncé", "Bhad Bhabie", "Bhaskar", "BIA", "Big Homie Ty.Ni",
                "Big Sean", "Big Wild", "BIGBANG", "Bigga Don", "BigKlit",
                "Billie Eilish", "Billy Crawford", "Billy Joel", "Billy Marchiafava", "BIMONTE",
                "Bishop Briggs", "Bizarrap", "Bizzy Crook", "Blac Youngsta", "black a.m.",
                "Black Eyed Peas", "blackbear", "BlackMayo", "BLACKPINK", "Blaikz",
                "Blaine", "Blanco", "Blanco Brown", "BLIND.SEE", "Blinded Hearts",
                "Blondes", "bloom", "Blu DeTiger", "BLUE ENCOUNT", "Blue Jeans",
                "Blueface", "BLUPRNT", "Blxst", "BLYMLDT", "BMW KENNY",
                "Bob Marley & The Wailers", "Bobby Shmurda", "Bodybangers", "BODYWORX", "Boehm",
                "Bonde R300", "Boney M.", "Bonnie Bailey", "Bop King", "Boring Lauren",
                "BØRNS", "Bosx1ne", "Boza", "Braaten", "Braaten & Chrit Leaf",
                "Brain Music", "Brando", "Brandon ThaKidd", "Brandy", "Brandz",
                "Breakbot", "BreaksManado", "Brent Faiyaz", "Brickboydior", "Bridgit Mendler",
                "Bring Me The Horizon", "Britney Spears", "BROCKHAMPTON", "BRONSON", "Brooks",
                "Brown Skies", "BRS Kash", "Bruno Mars", "BrxkenBxy", "Bryson Tiller",
                "BTS", "Bugoy Drilon", "bülow", "Burgos", "BURNOUT SYNDROMES",
                "Busta Rhymes", "BYNX", "C Guapo", "C.LACY", "C2d",
                "Cafe Disko", "Cage The Elephant", "CakeMan", "Calabasas", "Calboy",
                "Cali Swag District", "Callista Clark", "Cally Rhodes", "Calmani & Grey", "Calum Scott",
                "Calum Venice", "Calvin Harris", "CALVO", "Cameron Sanderson", "Camila Cabello",
                "Camilo", "Cap Carter", "Capital Cities", "Cardi B", "Carla",
                "Carlie Hanson", "Carly Rae Jepsen", "carolesdaughter", "Caroline Kole", "CARSTN",
                "Cartoons", "Cash Koo", "CashMoneyAp", "Cassie", "Cast - Sofia the First",
                "Caterina Valente", "Cdot Honcho", "Celeste Legaspi", "CELINE", "CG5",
                "Chaël", "Championxiii", "Chance the Rapper", "Charli XCX", "Charlie Sloth",
                "Charly Black", "Chase Atlantic", "CHASE B", "Cheflodeezy", "Chema Rivas",
                "Chester Young", "Chico Rose", "Chiiild", "Children Under the Sun", "Chimbala",
                "Chip Tha Ripper", "Chloe George", "Chloe x Halle", "Chord Overstreet", "Chris Brown",
                "Chris Lawyer", "Chrishan", "Christian French", "Christina Perri", "CHROMANCE",
                "Chuku100", "Chumino", "Chvnge Up", "Ciara", "Cico P",
                "Cigarettes After Sex", "Cinderella", "cinnamons", "City Girls", "City James",
                "CJ", "Claire Rosinkranz", "Clairo", "Claptone", "Claud Musik Indonesia",
                "Clean Bandit", "CLR", "CMTEN", "Cobra Starship", "Cochise",
                "Coco Lense", "Cody Simpson", "Coi Leray", "Coldplay", "Colone",
                "Color Theory", "Comethazine", "Compton Ro2co", "Conan Gray", "Conkarah",
                "Cookiee Kawaii", "Coolio", "Coopex", "Cordae", "Corinne Bailey Rae",
                "CORPSE", "Cosculluela", "Cosmo Sheldrake", "Courtney", "Cousin Stizz",
                "Coyote Theory", "Cozy Soundz", "CPX", "Craig David", "CRASH RARRI",
                "Crazy Frog", "Crisaunt", "CRISPIE", "Crissin", "Crystal Rock",
                "Cue C", "Cuebrick", "Cults", "Curtis Roach", "Curtis Waters",
                "Cuuhraig", "Cytus ll", "D-Rah", "D12", "D4L",
                "DaBaby", "Daddy Yankee", "Daddyphatsnaps", "Daft Punk", "Dagny",
                "Dame Dame", "Damien", "Dan + Shay", "DancingRoom", "Daniel Allan",
                "DankMemez", "Danko", "DankTiks", "Danny Aro", "Danny Avila",
                "Danny Joseph", "Dante", "Dante Boy", "Dante Klein", "Danz Dow",
                "DARKMARK", "Darling Brando", "Darwin", "Dastic", "Dathan",
                "Dave East", "DAVESTATEOFMIND", "Davey Langit", "David Guetta", "David Puentez",
                "David Shane", "David Shawty", "Dawin", "Daya Luz", "Dayme y El High",
                "Daz Payne", "DAZZ", "DDG", "deadman 死人", "Dean Lewis",
                "DeathbyRomy", "December Avenue", "Declan McKenna", "Deep Chills", "Deestroying",
                "DeeWunn", "DeJ Loaf", "Dem Franchize Boyz", "Dem Rap Bolz", "Demi Lovato",
                "Denari", "Denaron", "DenivMasadaMusic", "DENNIS", "Dermot Kennedy",
                "Devault", "dewolS Music", "Dexys Midnight Runners", "Didrick", "Die Antwoord",
                "Digga D", "Digrasso", "Dillon Francis", "Dimitri Vegas & Like Mike", "Dino Warriors",
                "Diplo", "Dirty Heads", "DIRTYXAN", "Disclosure", "Disco Fries",
                "Disco Lines", "Discofields", "Dixie", "Dixson Waz", "Diys",
                "dj 6rb", "DJ ARTHUZIIN", "DJ Bianda", "DJ Bryanflow", "DJ Challenge X",
                "DJ Chose", "DJ Cleitinho", "DJ Covy", "DJ Desa", "DJ DOLA do TRIÂNGULO",
                "Dj Douyin Remix", "DJ Drobitussin", "Dj EddyBeatz", "DJ Eezy", "DJ Enjel",
                "DJ Facemask", "DJ Gollum", "DJ Gotta", "DJ GRZS", "Dj Guuga",
                "DJ Haning", "Dj Imut", "Dj Jac", "Dj Kass", "DJ Khaled",
                "DJ Loonyo", "DJ Luc14no Antileo", "DJ Lucas Beat", "DJ Menor 7 DJ Menor da Dz7", "DJ Molasses",
                "DJ Nanda", "Dj Nbeat", "DJ Noiz", "DJ Ofreck", "DJ Opus",
                "DJ P13", "DJ Patrick Muniz", "Dj Peligro", "DJ Pro Code", "DJ Purpberry",
                "DJ Qhelfin", "DJ Quarantine", "Dj Raulito", "DJ Record", "DJ Red Core",
                "DJ Remix", "DJ Rowel", "Dj Rowel", "DJ Sauna", "DJ Sephora",
                "Dj Serpinha", "Dj Setia", "dj Shawny", "DJ Siul", "Dj Smith Casma",
                "DJ Souza Original", "DJ TikTok", "Dj Tiktokker", "DJ Viral", "Dj Vm",
                "Dj W-Beatz", "Dj Wesley Gonzaga", "DJ Wrekshop", "DJ Xavierj713", "Dj Yaksy",
                "Dj πrata", "DjDanz Remix", "DjKeinth", "DMNDS", "DNT RYE",
                "Doja Cat", "Dolo Tonight", "DOM SHWN", "Don Diablo", "Don Omar",
                "Don Toliver", "Donnalyn", "Drake", "Dramos", "Dre Banks",
                "Dre'es", "Drew", "Drinche", "DripReport", "Dro Kenji",
                "Dror", "Dua Lipa", "Dualities", "Dubdogz", "Dubskie",
                "Duce Mino", "Dugem Nation", "Duncan Laurence", "DUSTY LOCANE", "Dustystaytrue",
                "DVBBS", "Dyllón Burnside", "E the profit", "E-40", "Early Studios",
                "Eastblock Bitches", "Eazy-E", "Ebony", "Ed Helms", "Ed Sheeran",
                "Eddie Vedder", "EDEN", "Edith Whiskers", "Eduardo Luzquiños", "Edward Sharpe & The Magnetic Zeros",
                "EGOVERT", "Ekoh", "El Alfa", "EL Fresh", "El Memer",
                "El Nikko DJ", "El Papi", "El Villanord", "Elevating Sounds", "eli.",
                "elie curtis", "Elilluminari", "Elizabeth Wyld", "Ella Eyre", "Ella Mai",
                "Ella McCready", "ellee ven", "Ellie Goulding", "Eltee", "ElyOtto",
                "Em", "Eminem", "Emman", "Emotional Oranges", "Engelwood",
                "Enviousofenvy", "Eric Bellinger", "Erica Banks", "Eritza", "Estelle",
                "ETHN ASHR", "Etta James", "Ev0lution", "Eva Grace", "Eve",
                "Ex Battalion", "Eyedress", "Ez Mil", "EZRA", "F. Physical",
                "F3DE", "Facemask Time", "Fairview", "Fallen Roses", "Famous Dex",
                "Far East Movement", "Farruko", "Faruk Orman", "Fat Joe", "Febri Hands",
                "Fedd the God", "FEL!X", "Felicia Lu", "Felix Samuel", "Felix Schorn",
                "Felly", "Fendii AP", "Fergie", "Fetty Wap", "FiloChill",
                "Finn Askew", "Fire Jane", "Flapjax", "Fleetwood Mac", "FLETCHER",
                "Flo Milli", "Flo Rida", "flora cash", "Flow G", "Flow La Movie",
                "Floxyn", "FNF Chop", "Foolio", "Fools Garden", "Forrest.",
                "Foster", "Foster The People", "Fousheé", "FR!ES", "Frances Forever",
                "Frank Ocean", "FRED PANOPIO", "Freddie Dredd", "Freddy Verano", "Freischwimmer",
                "FreqLoad", "FreshDuzIt", "Freshie", "Freya Ridings", "Frostydasnowmann",
                "FRVRFRIDAY", "FSDW", "Fugees", "Funzo & Baby Loud", "Future",
                "G-Eazy", "Gabi Riveros", "Gabry Ponte", "Gagong Rapper", "Gaho",
                "Galantis", "Galwaro", "Gamma1", "GAMPER & DADONI", "Gary Valenciano",
                "Gaten Matarazzo", "GATTÜSO", "Gente De Zona", "Geo Ong", "George Michael",
                "Gera MX", "Getter Jaani", "Giang Pham", "Gigi D'Agostino", "Gill the ILL",
                "Gimme 5", "GIRLI", "Giuseppe Vittoria", "Giveon", "Gjesti",
                "Glaceo", "Glady's and the Boxers", "Glass Animals", "Glee Cast", "Global Dan",
                "Gloc 9", "Glockboyz Teejaee", "Gloria Estefan", "GoldLink", "Good Gas",
                "Goodboys", "Gooney Jib", "Gorillaz", "Gotye", "Griegz",
                "Gritty Lex", "Grouplove", "Grover Washington, Jr.", "Gryffin", "Gunna",
                "Gustixa", "Guthrie Nikolao", "Guy Sebastian", "Guyon Waton", "Gwen Stefani",
                "Gym Class Heroes", "H.E.R.", "Haise", "Haiti Babii", "Hale",
                "Halsey", "Handsome Luke", "Hard Lights", "Harmless", "Harris & Ford",
                "Harry Styles", "Haska", "HÄWK", "Hayd", "Hazel Faith",
                "HBz", "Hd4president", "Heartbreak Aris", "Hedley", "Henny Seear",
                "Henri Purnell", "Henry Hood", "HenryDaher", "Henyong Makata", "Herman",
                "Heron & Serum", "Hey Joe Show", "Hey Santana", "High Quality The Label LLC", "highonclouds",
                "Highup", "Hiko", "Hip Hop Harry", "Hippie Sabotage", "HL Wave",
                "Hobo Johnson", "Hollaphonic", "Honcho Moonk", "Honey Gee", "HONNE",
                "Hoobastank", "Hoodie Mitch", "Hotdog", "Hoxtones", "Hr. Troels",
                "HRVY", "Hudson East", "Huey V", "HUGEL", "Huncho Da Rockstar",
                "HUTS", "HVME", "Hwa Sa", "HXLLYWOOD", "HYO",
                "Hyperclap", "I Belong to the Zoo", "I Don't Speak French", "I.U", "Iamdoechii",
                "IAMJOSHSTONE", "iann dior", "Icona Pop", "Iggy Azalea", "Ijiboy",
                "ilham", "ILIRA", "Ilkan Gunuc", "Ilkay Sencan", "Ill Snek",
                "iLL Wayno", "Illijah", "iLOVEFRiDAY", "iLoveMemphis", "Imagine Dragons",
                "Imago", "Imanbek", "Imran Khan", "imy2", "Inigo Pascual",
                "INOJ", "Insane Clown Posse", "Internet Money", "Ir Sais", "Israel Del Amo",
                "Issam Alnajjar", "ItsLee", "ITZY", "Iwansteep Official", "Iyaz",
                "J Balvin", "J Boog", "J Swey", "J!mmy", "J. Cole",
                "J.I the Prince of N.Y", "Jack & James", "Jack Be", "Jack Daily", "Jack Harlow",
                "Jack Johnson", "Jack Stauber", "JACKBOYS", "Jacob Tillberg", "JACØBS",
                "Jacquees", "JADE MOSS", "Jaden", "Jae Trill", "Jamayne",
                "James Arthur", "James Bay", "James Blake", "James Carter", "James TW",
                "Janine Teñoso", "Jannis Block", "Japanic", "Jasiah", "Jason Derulo",
                "Jastefy", "Jawsh 685", "Jax Jones", "Jay Hoodie", "Jay Rock",
                "Jay Sean", "Jay Wheeler", "JAY-Z", "Jazmin Sisters", "JBK",
                "Je", "Jean Juan", "Jeaux", "Jelly Drops", "Jemme",
                "JenCee", "Jenil", "Jenny Jewel", "Jensi Jenno", "jeonghyeon",
                "Jeremih", "Jeremy Zucker", "Jerome", "Jersey Club", "Jess Benko",
                "Jessi", "JESSIA", "Jessica Hammond", "Jessie J", "Jethro",
                "Jewelz & Sparks", "Jhené Aiko", "Jhonni Blaze", "Jiaani", "JM Bales",
                "Joel Corry", "JoeVille", "Joey + Kelly Thompson", "Joey Melrose", "Joey Purp",
                "John Lennon", "John Mayer", "John Roa", "John The Blind", "John Williams",
                "Johnnie Mikel", "Johnny Orlando", "Join The Club", "Joji", "JoJo",
                "Jom", "Jon + Larsen", "Jon Eidson", "Joni Remix", "Jonn Hart",
                "Jordan Jay", "Jose De Las Heras", "Josefine", "Joseph Black", "Josh A",
                "Josiane Lessard", "Jowell & Randy", "Joy", "JP Saxe", "Jr Crown",
                "JRL", "Juan Caoile", "juan karlos", "Jubël", "Juice WRLD",
                "Julia Michaels", "Julieta Venegas", "June3rd", "Junie", "jusLo",
                "Just Hush", "Just Ice", "Justin Bieber", "Justin Morelli", "Justin Quiles",
                "Justin Timberlake", "Justin Vasquez", "Justin Wellington", "Justina Valentine", "JustRap",
                "JVKE", "JVLA", "jxdn", "Jyxo", "K And The Boxers",
                "K CAMP", "K-391", "K.A.A.N.", "K.Moses", "K'ron",
                "KA!RO", "Kaash Paige", "Kaayne", "KAAZE", "Kaito Shoma",
                "Kakaiboys", "Kali", "Kali Cass", "Kali Uchis", "Kamaiyah",
                "Kamil", "KANA-BOON", "Kane Brown", "Kanye West", "Kaphy",
                "Kapthenpurek", "Kardinal Offishall", "Karencitta", "Karl Wine", "KAROL G",
                "Karri", "Kash Doll", "Kat Meoz", "Kat Nova", "Katy Perry",
                "Katya Mila", "Kavale", "Kayla Nicole", "KAYTRANADA", "KAYY ELL",
                "KBFR", "KD One", "Kelis", "Kendra Erika", "Kendrick Lamar",
                "Kenjhons", "Kenndog", "Kensuke Ushio", "Kero Kero Bonito", "Kerry Wheeler",
                "Kesh Kesh", "Kesha", "keshi", "Kevin Cartoon", "Kevin Courtois",
                "Kevin Gates", "Kevin Powers", "Keyshia Cole", "Khia", "Kid Alina",
                "Kid Cudi", "Kid Francescoli", "Kid Ink", "Kid Travis", "Kidd Keo",
                "KIDS SEE GHOSTS", "KIIINGSAM", "Kikuo", "KillBunk", "KILLY",
                "Kim Chiu", "Kim Dracula", "Kina", "King Badger", "King Combs",
                "King Critical", "King Kaybee", "King Princess", "King Staccz", "King Tutt",
                "King Von", "KINGMOSTWANTED", "kingpuntocom beats", "Kings of Leon", "Kinneret",
                "Kir", "Kira P", "Kito", "Kitt Wakeley", "Kiubbah Malon",
                "Kiyo", "Kizz Daniel", "Klaas", "KLANG", "Klaus Badelt",
                "Klave", "Koda", "Kodak Black", "Kolohe Kai", "Kooly Bros",
                "Koosen", "Kortnee Simmons", "kostromin", "Kota Banks", "Krazymut",
                "Kreepa", "Kris Yute", "krisostomo", "KRYPTO9095", "KSHMR",
                "KSI", "Kule Ka$h", "Kumarion", "Kungs", "Kuya Bryan",
                "KVSH", "Kyan Palmer", "Kygo", "KYLE", "Kyle Echarri",
                "Kyle Exum", "Kyle Juliano", "Kyle The Hooligan", "Kyle Zagado", "L.Dre",
                "L'Trimm", "La Memerano", "La Nevula23 Productor", "LA Rodriguez", "La Roux",
                "Labrinth", "Lady Gaga", "Ladytron", "Lakim", "Lana Del Rey",
                "LANDR", "Landstrip Chip", "LANNÉ", "Lara Anderson", "Lara Silva",
                "Larissa Lambert", "Larray", "Larusso", "Lary Over", "Latto",
                "LAUWE", "Lavaado", "Lbdjs Record", "LBS Kee'vin", "Le Pedre",
                "le Shuuk", "Le Tigre", "Leat'eq", "Leeric", "Lele Pons",
                "Leo Santana", "Leon Jaar", "Leony", "Leska", "Lew Mr. Blue",
                "Lewis Capaldi", "Lexi Jayde", "Liam Payne", "Life Loops", "Lifs_a_Trip",
                "Likybo", "Lil Baby", "Lil Boom", "Lil Cray", "Lil Darkie",
                "Lil Dicky", "Lil Flop", "Lil Heavy Chainz", "Lil Jon", "Lil Kapow",
                "Lil Kayla", "Lil Keed", "LIL MAYO", "Lil Memer", "Lil Mosey",
                "Lil Nas X", "LIL NIX", "Lil Peep", "Lil Perfect", "Lil Rekk",
                "Lil Scrappy", "Lil Shock", "Lil Sil", "Lil Tecca", "Lil Tjay",
                "Lil Toe", "Lil Uzi Vert", "Lil Vinceyy", "Lil Wayne", "Lil Wil",
                "Lil Xxel", "Lil Yachty", "lilbootycall", "LILHUDDY", "Lilianna Wilde",
                "lilspirit", "Lily Allen", "Limón Limón", "Lindsey Stirling", "Linked Horizon",
                "Lion", "Lipless", "liquidfive", "LiSA", "Lit Killah",
                "Little Mix", "Little Monstaz", "Little Simz", "LIZ", "LIZOT",
                "Lizz Robinett", "Lizzo", "Lloyd", "LLusion", "Lofi Fruits Music",
                "Lonely God", "Lonr.", "Loonie", "Lord Huron", "Lorde",
                "Los Dioses Del Ritmo", "Los Farandulay", "Los Legendarios", "Lost People", "Lotus",
                "Lou Bega", "Loui", "Louis Tomlinson", "Love Harder", "LPB Poody",
                "LT", "Lucas & Steve", "Lucas Coly", "Lucas Estrada", "Lucio",
                "Lucky Luke", "Ludacris", "Ludvigsson", "Luh Baam", "Luh Kel",
                "Luki", "LUM!X", "LUNAX", "Lund", "LVP",
                "Lykke Li", "Lyss", "M Fair", "M83", "Mabel",
                "Mac Miller", "Macaulay Sulkin", "Machine Time", "Macklemore", "Macklemore & Ryan Lewis",
                "Mad Dogz", "Madcon", "Madism", "Madison Beer", "Maejor",
                "Maggie Lindemann", "Magnus Haven", "Mahogany Lox", "Mahout", "Maikitol",
                "Majestic", "Major Lazer", "Mak Sauce", "Mala Agatha", "Malu",
                "Maluma", "Mangoo", "MANILA GREY", "Marc Benjamin", "Marc E. Bassy",
                "Marc Korn", "MarcLegend", "Marco Nobel", "Marcus & Martinus", "Marcus Layton",
                "Mariah Carey", "MARINA", "Marion Aunor", "Mark B.", "Mark Carpio",
                "Mark Mendy", "Maroon 5", "Mars Wagon", "Marshmello", "Martin Arteta",
                "Martin Garrix", "Martin Jensen", "Martin van Lectro", "Marv Allen", "Marwa Loud",
                "Mary Jane Girls", "Mary Mary", "Masatoshi Ono", "Masego", "Masked Wolf",
                "MASN", "Masove", "Master KG", "Mathias.", "Mati Masildo",
                "Matoma", "Matthaios", "Matthew Wilder", "Matvey Emerson", "Mauria",
                "Màvcase", "Maxence Cyrin", "Maximillian", "Maximo", "MaxsIndBass",
                "Maykel Mantow", "Mayonnaise", "Mazen", "MC 3L", "Mc Abalo",
                "MC Cego Abusado", "MC CH da Z.O", "Mc Davi", "Mc Draak", "MC Einstein",
                "Mc Kaio", "Mc Kevin", "MC Kevin o Chris", "Mc Lele", "Mc Livinho",
                "MC Magrella", "MC Marley", "MC Matheuzinho", "MC Meno K", "MC MN",
                "Mc Rennan", "MC Torugo", "MC Virgins", "Mc Zaac", "MC Zaquin",
                "MC4D", "MEDUZA", "Mega Shinnosuke", "Megan Thee Stallion", "Meghan Trainor",
                "Melanie Martinez", "Melonia", "MePemuro", "Merty Shango", "Meryl Streep",
                "Mesto", "metr", "Metrixx", "Metro Boomin", "Metro Station",
                "MGMT", "Mi Pan Ñam Ñam", "Micast", "Michael Bars", "Michael Dutchi Libranda",
                "Michael Jackson", "Michael Pangilinan", "Midi Blosso", "Mighty Bay", "Migos",
                "Migrantes", "Miguel", "Mike Candys", "Mike Dimes", "Mike La Funk",
                "Mike Posner", "Mike Vallas", "Miley Cyrus", "Millie B", "Milly",
                "mimi bay", "Mimi Webb", "mimiyuuuh", "Minerro", "Missy Elliott",
                "Mitski", "Miura Jam", "MixAndMash", "MkX", "Mo2crazee",
                "Mobbstarr", "Mobile Legends: Bang Bang", "Moguai", "Mohead Mike", "Mohombi",
                "Moira Dela Torre", "MOKABY", "MOL$", "Molly Brazy", "Momokurotei Ichimon",
                "Money Man", "Moneybagg Yo", "MoneyMarr", "Monica", "Monte Booker",
                "Moon OA", "Moonstar88", "Mooski", "Mora", "More Fatter",
                "MORGENSHTERN", "Morissette", "Mother Mother", "MOTi", "Mozzy",
                "Mr_hotspot", "Mr. 2-17", "Mr. Budots", "Mr. C", "Mr. Pig",
                "Mrs. GREEN APPLE", "Ms.OOJA", "Muffin", "Mulherin", "Mumford & Sons",
                "Muse", "Music Hero", "Mustard", "Myke Towers", "Nadine Lustre",
                "Nadson O Ferinha", "Naeleck", "Nakala", "Namelle", "Nanda Lia",
                "Nardo Wick", "Natalie Taylor", "Natasha Mosley", "Nathan Evans", "Natio",
                "Natti Natasha", "NaXwell", "Nazia Marwiana", "Ne-Yo", "Nea",
                "Near", "Nelly", "Nelly Furtado", "Nene Malo", "Neoni",
                "Neptunica", "Ness The Kid", "Netta", "NextYoungin", "NF",
                "Nfasis", "Nick Cave & The Bad Seeds", "Nick Le Funk", "Nicki Minaj", "Nicky Romero",
                "Nicole Chambers", "nicopop.", "Nightmare", "Niiko x SWAE", "Nik Makino",
                "Niko B", "Nio Garcia", "Nippandab", "NLE Choppa", "No Genre",
                "Noah Cyrus", "Noble Lyfe", "Nodis", "Nofin Asia", "Noisettes",
                "Nora Van Elken", "Number9ok", "NvMe", "O Masokista", "O.C. Dawgs",
                "ocean", "Ocho Drippin", "Octavian", "Ofenbach", "offrami",
                "Offset", "OG Gazo", "Okean Elzi", "OLA RUNT", "Oliver Heldens",
                "Olivia Newton-John", "Olivia O'Brien", "Olivia Rodrigo", "Olympis", "Omarion",
                "OMC", "Omega", "omgkirby", "OpenSoul", "Orange & Lemons",
                "Osocali", "OT BEATZ", "Otavio Beats", "Outkast", "oxy_gin",
                "Ozuna", "P!nk", "Papa Zeus", "Paper Idol", "papichuloteej",
                "Papu DJ", "Parari", "Paris Boy", "Party Favor", "Pascal Letoublon",
                "Past Present", "Pateezy", "PatrickReza", "Paul Anka", "Paul BornaStar",
                "PDL", "Peach Tree Rascals", "Peachy!", "PEDRO", "PEDRO SAMPAIO",
                "Peewee Longway", "Perfect Pitch", "Peter Kuli", "PETER LAKE", "Peter Manos",
                "Pháo", "Phay", "Phil The Beat", "Phillip Phillips", "PhoMeme",
                "Pia Mia", "Picco", "Pink Chanel Dior", "Pink Guy", "Pink Sweat$",
                "Piso 21", "Pitbull", "PiuPiu", "Pixies", "Plain White T's",
                "Plan B", "Playboi Carti", "Pleasure P", "Plies", "Plustwo",
                "PLVTINUM", "PmBata", "PnB Rock", "POCAH", "Polo & Pan",
                "Polo Frost", "Polo G", "Pooh Shiesty", "Pop Smoke", "Popp Hunna",
                "Porter Robinson", "Post Malone", "Power Beats Club", "Powfu", "ppcocaine",
                "Precious Brown", "Priceless Da Roc", "Prince of Falls", "Prince Peezy & Lala Chanel", "Prince Royce",
                "Princess Nokia", "Princess Thea", "Pronto Spazzout", "Prophet the Artist", "Psychedelic Boyz",
                "Ptazeta", "PUBLIC", "Pulsedriver", "Puri", "Purple Disco Machine",
                "Pusher", "Putih Abu-Abu", "Pyrex Pryce", "PYT. NY", "qtpie",
                "Quality Control", "Quavo", "Que 9", "Que Fieri", "QUE.",
                "Queen", "Qveen Herby", "R. Kelly", "R.I.O.", "R3HAB",
                "Rachel Lorin", "Rachel Platten", "Radical Face", "RADWIMPS", "Rae Rae",
                "Rae Sremmurd", "Raf Davis", "Rah Swish", "Rainych", "Rak-Su",
                "Ramz", "Randarzky", "Rane Marie", "Rarin", "Rasmus Gozzi",
                "Rasster", "Rat City", "Rauw Alejandro", "Rawi Djafar", "Ray J",
                "Ray Le Fanue", "Ray Moon", "Rayface", "RC", "RC Blizz",
                "Redbone", "Redfoo", "Regard", "Reggie Mills", "Reik",
                "Remi Wolf", "Remix Kingz", "Rennan da Penha", "Reo Brothers", "reptilelegit",
                "Revelries", "Rex Orange County", "REY VALERA", "Reyanna Maria", "Reyn Hartley",
                "Rich Homie Quan", "Rich Music LTD", "Rich The Kid", "Rick James", "Ricky Desktop",
                "Ricky Montgomery", "Rico Pressley", "Riff Raff", "Riflman", "Rihanna",
                "Ringnes-Ronny", "Rio", "Riotron", "Riton", "Ritt Momney",
                "Rivertree Music", "Rizky Ayuba", "Rizzy Rackz", "Roar", "Rob Deniel",
                "Rob V", "Robaer", "Robbe", "Robbie Doherty", "Robbie Williams",
                "Robert Vargas", "Robin M", "Robin Schulz", "Rochy RD", "Rod Wave",
                "Roddy Ricch", "Roel Cortez", "Roji", "Rolipso", "Rolling Stone P",
                "Roman Yasin", "Rompasso", "Ron Grams", "Ron Henley", "Rontae Don't Play",
                "ROSALÍA", "Rot Ken", "Roundrobin", "Roxie", "RU$$OV",
                "Ruan Nagel", "RudeBoyKels", "Run–D.M.C.", "RuskieBanana", "Russ",
                "Ruth B.", "Rvssian", "Ryan Celsius Sounds", "Ryan Shade", "Rylo Rodriguez",
                "Ryn Weaver", "S1mba", "Sabby Sousa", "Sad Puppy", "Sada Baby",
                "Sadfriendd", "Sage The Gemini", "SahBabii", "SAINt JHN", "SAKVREYE",
                "Salatiel", "salem ilese", "SALES", "Salt-N-Pepa", "salvia palth",
                "Sam Bird", "Sam Concepcion", "Sam Feldt", "Sam Fischer", "Sam Smith",
                "Sampaguita", "Sanjoy", "Sara Kays", "Sarah Almoril", "Sarah Geronimo",
                "Sasha Saidin", "Saweetie", "SAYGRACE", "SB NewGen", "Scarlet Pleasure",
                "Scarlett Taylor", "ScHoolboy Q", "Scooter", "Scotty", "Sean Finn",
                "Sean Kingston", "Sean Paul", "SEB", "Sebastian Rhodes", "Sebastian Yatra",
                "Sech", "Seeb", "Selena Gomez", "Selphius", "SelteMemset",
                "Sem", "Semme", "Seniel Music", "Senior", "Serena Isioma",
                "Sevyn Streeter", "Sezzy", "SGE Kash", "Shadow Cliq", "Shaggy",
                "Shakira", "Shakka", "Shaky Vibes", "ShalomTeck", "Shanti Dope",
                "SHAUN", "Shaun Reynolds", "Shawn Mendes", "Shayne Orok", "Sheck Wes",
                "Shehyee", "Sheppard", "Sherwyn", "Shiko Nightcore", "Shou",
                "Showtek", "Shumy Luke", "Shygirl", "Sia", "SICKOTOY",
                "Sigurd Barrett", "Silent Child", "Silent Sanctuary", "Simple Plan", "SimxSantana",
                "sinakun", "SINAN", "Sinoda", "Sir Chloe", "Sir Trilli",
                "Sir Winz", "Sista Prod", "SISTAR19", "Ski Mask The Slump God", "Skusta Clee",
                "Sky Kid", "Skylar Stecker", "SLANDER", "Slapaholics", "Sleepy Chows",
                "Sleepy Hallow", "Slikk Eastwood", "Slim K", "Slowed Music", "SlowMemer",
                "Smooth Vanilla", "Smugglaz", "Snakehips", "Snoop Dogg", "Social House",
                "SoFaygo", "Soft Q", "Sohodolls", "Solstice", "SOMI",
                "SOMMA", "Sondr", "Sonia Stein", "Sonya Belousova", "SoulChef",
                "Soulja Boy", "Soulstice", "Soulthrll", "Soundofyou", "SpaceMan Zack",
                "Speed Gang", "SPENCE", "SpotemGottem", "Ss.hh.a.n.a", "Star Music Artist",
                "StarBoi3", "Starley", "Starz", "StaySolidRocky", "STEEL",
                "Stef", "Steff da Campo", "Stefflon Don", "Stegosaurus Rex", "Stella Jang",
                "Stellar", "Stephany Joanna", "Steve Aoki", "Steve Brian", "Steve Lacy",
                "Steve Void", "Steven Universe", "Steven Young", "Stileto", "Stisema",
                "Stonebwoy", "Strange Fruits Music", "Strawberry Guy", "Studio Killers", "Stunna Bam",
                "Stunna Gambino", "Stunt DeGrate", "Stunt N Dozier", "Stupid Goldfish", "Sub Urban",
                "SUD", "Sueco", "Sugar Jesus", "sumika", "Summer Walker",
                "SummerDrives", "Super Yei", "Surf Curse", "Surf Mesa", "Surfaces",
                "Svniivan", "SyKo", "SYML", "SZA", "T H R O N E",
                "T-Pain", "T-Speed & 5upamanHOE", "T-Wayne", "t.A.T.u.", "T3",
                "Tai Verdes", "Tainy", "Tame Impala", "Tamia", "Tate McRae",
                "Tay Money", "Tay-K", "Taylor Swift", "Tayy Brown", "tearo, the ghost",
                "Tedy", "Tekla", "TELYKast", "TEMPOREX", "Terelulopo",
                "Terry Tesla", "Tesher", "Tessa Violet", "Tewy", "Th CDM",
                "The Art Of Noise", "The Backyardigans", "The Band Perry", "The Boyfriends", "THE BOYZ",
                "The Cardigans", "The Chainsmokers", "The Dance Queen Group", "The Execs", "The FifthGuys",
                "The Future Kingz", "The Hollister Jean Lab", "The Juans", "The Kid LAROI", "The Living Tombstone",
                "The Lumineers", "The Marimba Squad", "The Neighbourhood", "The Notorious B.I.G.", "The Paper Flowers",
                "The Prince Karma", "The Pussycat Dolls", "The Remix Guys", "THE SCOTTS", "The Script",
                "The Strokes", "The Strumbellas", "The Suncatchers", "The Underdog Project", "The Vamps",
                "The Victor", "The Weeknd", "The Wombats", "Thiaguinho MT", "Thierry Von Der Warth",
                "This Band", "thmpsn", "Thomas Reid", "Thundercat", "Thyro",
                "Tia", "Tiagz", "Tierra Whack", "Tiësto", "TikTokCenter",
                "Timbaland", "Timmy Thomas", "Timmy Trumpet", "Tinashe", "tinguspingus",
                "Tiny Meat Gang", "Tion Wayne", "Tippy Dos Santos", "TisaKorean", "Tiscore",
                "TIX", "TJ Monterde", "TK from Ling tosite sigure", "TK N Cash", "TMW",
                "tobi lou", "Toby Fox", "TOD", "Todrick Hall", "TOKYO’S REVENGE",
                "Tom Jones", "Tom Odell", "Tom Walker", "Tomcraft", "Tommee Profitt",
                "Tones And I", "Tony Igy", "Tony Willrich", "TOO", "Top5",
                "Topic", "TorontoMusicPlug", "Tory Lanez", "Tove Lo", "Tove Styrke",
                "Trap Beckham", "trapwithjames", "Travis Porter", "Travis Scott", "Tre Coast",
                "Tre Savage", "Trevor Daniel", "Trevor Wesley", "Trey Forever", "Trey Songz",
                "Trill Ryan", "Trilly Trills", "Trippie Redd", "Trouze", "Troye Sivan",
                "Tungevaag", "Tusse", "TV Girl", "TWICE", "Twista",
                "Twisted Harmonies", "Two Feet", "Two Maloka", "Two Neighbors", "twocolors",
                "TWOPILOTS", "Ty Dolla $ign", "Tyga", "Tyler April", "Tyler, The Creator",
                "Tz da Coronel", "TZAR", "Ugly God", "UglyFace", "Ultradiox",
                "Ummet Ozcan", "Unghetto Mathieu", "United Idol", "Up Dharma Down", "UPSAHL",
                "Usher", "V.I.C.", "Valentino Khan", "VAMERO", "Vampire Weekend",
                "Vanessa Carlton", "Vanfire", "Vaundy", "Vazer", "Vedo",
                "Vegedream", "Vengaboys", "Venti", "VGR", "Vibe Street",
                "Vicente Mejillano Jr", "Vicetone", "Victoria Monét", "VIEGO", "Vierre Cloud",
                "VINAI", "Vinny West", "Vinoh", "Vion Konger", "Viral DJs",
                "Visualz By Mariee", "Vito V", "Viva Hot Babes", "Viva La Panda", "VIZE",
                "VMZ", "Volac", "VST & Company", "VVS Collective", "W&W",
                "Wabi Sabi", "Waka Flocka Flame", "Wale", "Wallows", "wanderoof",
                "Weezer", "Weirdo King", "West Monroe", "WHATEVER WE ARE", "Whigfield",
                "WhiteCapMusic", "Whitney Houston", "WhoHeem", "Wight Kid", "Wiill",
                "Will Joseph Cook", "Will Ryte", "Will Smith", "WilliamGregory", "WILLOW",
                "Willy Anggawinata", "Wisin", "Wiz Khalifa", "WizKid", "Woodii",
                "Wyatt Thunder", "X Ambassadors", "X Lovers", "XANAKIN SKYWOK", "Xand Avião",
                "Xanity", "Xb Gang", "Xilo", "xotrapp", "XXXTENTACION",
                "Y2K", "Yakusaaa", "Yandro El Protagonista", "Yannc", "yaveco",
                "Yayoi", "YBN Nahmir", "Years & Years", "Yeng Constantino", "Yes",
                "YFN Lucci", "YG", "Ying Yang Twins", "YK Osiris", "YN Jay",
                "YNG Martyr", "YNW Melly", "Yo Gotti", "YOASOBI", "Yoh kamiyama",
                "yoitsvic", "Yolanda Be Cool", "Yorii", "Yoshihisa Hirano", "Yot Club",
                "Young B", "Young Fanatic", "Young Fresh", "Young M.A", "Young Maylay",
                "Young Nudy", "Young T & Bugsey", "Young Thug", "Young Tone", "YoungBoy Never Broke Again",
                "Younotus", "YSB Eli", "Ysl Jalen", "Yumi Lacsamana", "Yung Baby Tate",
                "Yung Bae", "Yung Bleu", "Yung Gravy", "YUNG NATION", "Yung Nazty",
                "Yung Pinch", "Yung Pooda", "Yung Raja", "Yung Skrrt", "Yung Yung Pallo",
                "YUNGBLUD", "Yungeen Ace", "YungManny", "Yungster Jack", "Yves V",
                "Z. Weina", "Zacai", "Zach Farache", "Zack Tabudlo", "ZaeHD & CEO",
                "ZAYN", "Zedd", "Zephanie", "Zita", "Zoe Wees",
                "Zombic", "Zum", "ギヴン", "クラピカ(CV:沢城みゆき)", "たかやん",
                "ヒソカ(CV:浪川大輔)", "小潘潘", "物語シリーズ", "稲葉曇", "美波",
                "音阙诗听"
        };

        int columns = 4;
        int rows = (int) Math.ceil((double) artists.length / columns);
        int maxLength = 0;
        for (String artist : artists) {
            maxLength = Math.max(maxLength, artist.length());
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                int index = i + j * rows;
                if (index < artists.length) {
                    String artist = artists[index];
                    System.out.printf("%-" + (maxLength + 2) + "s", artist);
                }
            }
            System.out.println();
        }
        System.out.println("-------------------------------------------------------------------------------------------------------------------------------------");
        System.out.print(" Input Choice: ");
        String choice = scanner.nextLine();

        double totalLoudness = 0;
        int numberOfSongs = 0;

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] col;
                if (!line.contains("\"")) {
                    col = line.split(",");
                } else {
                    col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); // ignoring commas inside quotation marks
                }

                if (col[3].equalsIgnoreCase(choice)) {
                    numberOfSongs++;

                    try {
                        double loudness = Double.parseDouble(col[11]);
                        totalLoudness += loudness;
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (numberOfSongs > 0) {
            double loudnessAve = totalLoudness / numberOfSongs;
            System.out.println("\n\n======================================================");
            System.out.println("       AVERAGE LOUDNESS OF SONGS BY " + choice);
            System.out.println("======================================================");
            System.out.println("            " + loudnessAve);
        } else {
            System.out.println("No songs found for the selected artist.");
        }
    }
}

