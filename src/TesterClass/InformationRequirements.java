package TesterClass;

/**
 * The reference class used for the main classes of Activity 0
 */
public class InformationRequirements {
    private String trackID;
    private String genre;
    private String artistID;
    private String artistName;

    private String releaseDate;
    private String trackName;
    private String albumID;
    private String playlistID;
    private String playlistName;

    private String durationMins;
    private String durationMilliSecs;
    private String popularity;

    private String key;
    private String loudness;
    private String mode;
    private String speechiness;
    private String acousticness;

    private String instrumentalness;
    private String valence;

    private String danceability;
    private String energy;
    private String liveness;
    private String tempo;

    public String getTrackID() {
        return trackID;
    }

    public String getGenre() {
        return genre;
    }

    public String getArtistID() {
        return artistID;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getAlbumID() {
        return albumID;
    }

    public String getPlaylistID() {
        return playlistID;
    }

    public String getPlaylistName() {
        return playlistName;
    }

    public String getDurationMins() {
        return durationMins;
    }

    public String getDurationMilliSecs() {
        return durationMilliSecs;
    }

    public String getPopularity() {
        return popularity;
    }

    public String getKey() {
        return key;
    }

    public String getLoudness() {
        return loudness;
    }

    public String getMode() {
        return mode;
    }

    public String getSpeechiness() {
        return speechiness;
    }

    public String getAcousticness() {
        return acousticness;
    }

    public String getInstrumentalness() {
        return instrumentalness;
    }

    public String getValence() {
        return valence;
    }

    public String getDanceability() {
        return danceability;
    }

    public String getEnergy() {
        return energy;
    }

    public String getLiveness() {
        return liveness;
    }

    public String getTempo() {
        return tempo;
    }

    public InformationRequirements(String trackID, String trackName, String artistID, String artistName, String albumID, String durationMilliSecs, String releaseDate, String popularity, String danceability, String energy, String key, String loudness, String mode, String speechiness, String acousticness, String instrumentalness, String liveness, String valence, String tempo, String playlistID, String playlistName, String durationMins, String genre) {
        this.trackID = trackID;
        this.artistID = artistID;
        this.artistName = artistName;
        this.releaseDate = releaseDate;
        this.trackName = trackName;
        this.albumID = albumID;
        this.playlistID = playlistID;
        this.playlistName = playlistName;
        this.durationMins = durationMins;
        this.durationMilliSecs = durationMilliSecs;
        this.popularity = popularity;
        this.key = key;
        this.loudness = loudness;
        this.mode = mode;
        this.speechiness = speechiness;
        this.acousticness = acousticness;
        this.instrumentalness = instrumentalness;
        this.valence = valence;
        this.danceability = danceability;
        this.energy = energy;
        this.liveness = liveness;
        this.tempo = tempo;
        this.genre = genre;
    }//end of constructor

    public String toString() {
        return "\nTesterClass.InformationRequirements {" +
                "\ntrackID = " + trackID +
                "\ntrackName = " + trackName +
                "\nartistID = " + artistID +
                "\nartistName = " + artistName +
                "\nreleaseDate = " + releaseDate +
                "\nalbumID = " + albumID +
                "\nplaylistID = " + playlistID +
                "\nplaylistName = " + playlistName +
                "\ndurationMins = " + durationMins +
                "\ndurationMilliSecs = " + durationMilliSecs +
                "\npopularity = " + popularity +
                "\nkey = " + key +
                "\nloudness = " + loudness +
                "\nmode = " + mode +
                "\nspeechiness = " + speechiness +
                "\nacousticness = " + acousticness +
                "\ninstrumentalness = " + instrumentalness +
                "\nvalence = " + valence +
                "\ndanceability = " + danceability +
                "\nenergy = " + energy +
                "\nliveness = " + liveness +
                "\ntempo = " + tempo +
                "\ngenre = " + genre +
                '}';
    }//end of toString method

    public InformationRequirements(String trackName, String artistName, String albumID, String releaseDate,String popularity,String genre) {
        this.trackName = trackName;
        this.artistName= artistName;
        this.albumID = albumID;
        this.releaseDate = releaseDate;
        this.popularity =popularity;
        this.genre = genre;

    }
}//end of class
