package TesterClass;

public class toSort implements Comparable<toSort>{
    private String artist;
    private String track;
    private String release;
    private String genre;

    public toSort(String artist, String track, String release, String genre){
        this.artist = artist;
        this.track = track;
        this.release = release;
        this.genre = genre;
    }

    public String getArtist(){
        return artist;
    }

    public String getTrack(){
        return track;
    }

    public String getRelease(){
        return release;
    }

    public String getGenre(){
        return genre;
    }

    public String toString(){
        return getArtist() + " " + getTrack() + " " + getRelease()
                + " " + getGenre() + "\n";
    }

    @Override
    public int compareTo(toSort o) {
        return release.compareTo(o.release);
    }
}
