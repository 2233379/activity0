package TesterClass;


import java.io.*;
import java.util.Scanner;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;


public class TristanTest {
    static Scanner sc = new Scanner(System.in);
    static String frequencyDataset = "src/res/FrequencyRange2.csv";
    static String line; // Space
    static String colorGreen = "\u001B[32m";
    static String colorYellow = "\u001B[33m";
    static String colorReset = "\u001B[0m";

    public static void main(String[] args) {
        TristanTest tristanTester = new TristanTest(); // Creates object for TristanTester class
        tristanTester.menu();
    } // End of main method


    public void menu() {
        System.out.println(colorGreen + "\n============================================================================================" + colorReset);
        System.out.print(colorYellow + "1. Overall Tiktok Artist Frequency\n2. Frequency of an Artist per Genre\n3. Most Frequent Key Used" +
                "\n4. Ascending Order of Songs Released\n5. View Genre Frequency\n" + colorReset);
        System.out.println(colorGreen + "============================================================================================" + colorReset);
        System.out.print("Select a function: ");
        int response = Integer.parseInt(sc.nextLine());

        switch (response){
            case 1:
                System.out.println("\nOverall Tiktok Artist Frequency");
                try {
                    BufferedReader br = new BufferedReader(new FileReader(frequencyDataset));
                    System.out.println(colorGreen + "=======================================================" + colorReset);
                    while ((line = br.readLine()) != null) {
                        String[] col;
                        if (!line.contains("\"")) {
                            col = line.split(","); // Change to , later
                        } else {
                            col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); // ignoring commas inside quotation marks
                        }
                        System.out.printf("%1s%1s%1s%-25s%-1s%-5s%-1s%-20s%-5s%-1s%n", colorGreen, " | " , colorReset , col[1] , colorGreen , " | " , colorReset , col[2] , colorGreen, " | " , colorReset, "\n");
                        System.out.println(colorGreen + "-------------------------------------------------------" + colorReset);
                    } // End of while loop
                    System.out.println(colorGreen + "=======================================================" + colorReset);
                    menu();
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                } // End of try-catch
                break;
            case 2:
                System.out.println("\nFrequency of an Artist per Genre");
                System.out.println(" 1. TIKTOK DANCE\n2. TIKTOK OPM\n3. TIKTOK PHILIPPINES\n4. _TIKTOK\n");
                System.out.print("Select a genre: ");
                int genre = Integer.parseInt(sc.nextLine());

                switch (genre){
                    case 1:
                        try {
                            BufferedReader br = new BufferedReader(new FileReader(frequencyDataset));
                            System.out.println(colorGreen + "=======================================================" + colorReset);
                            while ((line = br.readLine()) != null) {
                                String[] col;
                                if (!line.contains("\"")) {
                                    col = line.split(","); // Change to , later
                                } else {
                                    col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); // ignoring commas inside quotation marks
                                }
                                System.out.printf("%1s%1s%1s%-25s%-1s%-5s%-1s%-20s%-5s%-1s%n", colorGreen, " | " , colorReset , col[11] , colorGreen , " | " , colorReset , col[12] , colorGreen, " | " , colorReset, "\n");
                                System.out.println(colorGreen + "-------------------------------------------------------" + colorReset);
                            } // End of while loop
                            System.out.println(colorGreen + "=======================================================" + colorReset);
                            br.close();
                            menu();
                            break;
                        } catch (IOException e) {
                            e.printStackTrace();
                        } // End of try-catch
                        break;
                    case 2:
                        try {
                            BufferedReader br = new BufferedReader(new FileReader(frequencyDataset));
                            System.out.println(colorGreen + "=======================================================" + colorReset);
                            while ((line = br.readLine()) != null) {
                                String[] col;
                                if (!line.contains("\"")) {
                                    col = line.split(","); // Change to , later
                                } else {
                                    col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); // ignoring commas inside quotation marks
                                }
                                System.out.printf("%1s%1s%1s%-25s%-1s%-5s%-1s%-20s%-5s%-1s%n", colorGreen, " | " , colorReset , col[13] , colorGreen , " | " , colorReset , col[14] , colorGreen, " | " , colorReset, "\n");
                                System.out.println(colorGreen + "-------------------------------------------------------" + colorReset);
                            } // End of while loop
                            System.out.println(colorGreen + "=======================================================" + colorReset);
                            menu();
                            break;
                        } catch (IOException e) {
                            e.printStackTrace();
                        } // End of try-catch
                        break;
                    case 3:
                        System.out.println("TIKTOK PHILIPPINES");

                        try {
                            BufferedReader br = new BufferedReader(new FileReader(frequencyDataset));
                            System.out.println(colorGreen + "=======================================================" + colorReset);
                            while ((line = br.readLine()) != null) {
                                String[] col;
                                if (!line.contains("\"")) {
                                    col = line.split(","); // Change to , later
                                } else {
                                    col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); // ignoring commas inside quotation marks
                                }
                                System.out.printf("%1s%1s%1s%-25s%-1s%-5s%-1s%-20s%-5s%-1s%n", colorGreen, " | " , colorReset , col[4] , colorGreen , " | " , colorReset , col[5] , colorGreen, " | " , colorReset, "\n");
                                System.out.println(colorGreen + "-------------------------------------------------------" + colorReset);
                            } // End of while loop
                            System.out.println(colorGreen + "=======================================================" + colorReset);
                            System.out.println("The Key of C Major or the Key Numeric Value 1 has the highest key frequency");
                            menu();
                            break;
                        } catch (IOException e) {
                            e.printStackTrace();
                        } // End of try-catch
                        break;
                    case 4: //Comments will be added later
                        try {
                            BufferedReader br = new BufferedReader(new FileReader(frequencyDataset));
                            System.out.println(colorGreen + "=======================================================" + colorReset);
                            while ((line = br.readLine()) != null) {
                                String[] col;
                                if (!line.contains("\"")) {
                                    col = line.split(","); // Change to , later
                                } else {
                                    col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); // ignoring commas inside quotation marks
                                }
                                System.out.printf("%1s%1s%1s%-25s%-1s%-5s%-1s%-20s%-5s%-1s%n", colorGreen, " | " , colorReset , col[17] , colorGreen , " | " , colorReset , col[18] , colorGreen, " | " , colorReset, "\n");
                                System.out.println(colorGreen + "-------------------------------------------------------" + colorReset);
                            } // End of while loop
                            System.out.println(colorGreen + "=======================================================" + colorReset);
                            menu();
                            break;
                        } catch (IOException e) {
                            e.printStackTrace();
                        } // End of try-catch
                        break;
                }
            case 3:
                System.out.println("\nMost Frequent Key Used");
                System.out.println("C Major = 0" +
                        "G Major = 1" +
                        "D Major = 2" +
                        "A Major = 3" +
                        "E Major = 4" +
                        "B Major = 5" +
                        "F# Major = 6" +
                        "C# Major = 7" +
                        "G# Major = 8" +
                        "D# Major = 9" +
                        "A# Major = 10" +
                        "E# Major = 11");
                try {
                    BufferedReader br = new BufferedReader(new FileReader(frequencyDataset));
                    while ((line = br.readLine()) != null) {
                        System.out.println(colorGreen + "=======================================================" + colorReset);
                        String[] col;
                        if (!line.contains("\"")) {
                            col = line.split(","); // Change to , later
                        } else {
                            col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); // ignoring commas inside quotation marks
                        }
                        System.out.printf("%1s%1s%1s%-25s%-1s%-5s%-1s%-20s%-5s%-1s%n", colorGreen, " | " , colorReset , col[4] , colorGreen , " | " , colorReset , col[5] , colorGreen, " | " , colorReset, "\n");
                        System.out.println(colorGreen + "-------------------------------------------------------" + colorReset);
                    } // End of while loop
                    System.out.println(colorGreen + "=======================================================" + colorReset);
                    menu();
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                } // End of try-catch
                break;
            case 4:
                System.out.println("\nSorted Songs Released in Ascending Order");
                File file = new File("src/res/sortByYear.csv");
                Set<toSort> sort= new TreeSet<>();

                try {
                    Scanner sc = new Scanner(file);
                    System.out.println(colorGreen + "============================================================================================" + colorReset);
                    while (sc.hasNextLine()) {
                        String[] col;
                        if (!line.contains("\"")) {
                            col = line.split(","); // Change to , later
                        } else {
                            col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); // ignoring commas inside quotation marks
                        }
                        sort.add(new toSort(col[0], col[1], col[2], col[3]));
                        System.out.println(colorGreen + "-------------------------------------------------------" + colorReset);
                    }
                    System.out.println(colorGreen + "=======================================================" + colorReset);
                }catch (FileNotFoundException e){
                    e.printStackTrace();
                }
                System.out.println(sort);
                System.out.println("\nReturning to menu...");
                menu();
            case 5:
                System.out.println("\nDisplay Trending Genre");
                try {
                    BufferedReader br = new BufferedReader(new FileReader(frequencyDataset));
                    System.out.println(colorGreen + "=======================================================" + colorReset);
                    while ((line = br.readLine()) != null) {
                        String[] col;
                        if (!line.contains("\"")) {
                            col = line.split(","); // Change to , later
                        } else {
                            col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); // ignoring commas inside quotation marks
                        }
                        System.out.printf("%1s%1s%1s%-25s%-1s%-5s%-1s%-20s%-5s%-1s%n", colorGreen, " | " , colorReset , col[7] , colorGreen , " | " , colorReset , col[8] , colorGreen, " | " , colorReset, "\n");
                        System.out.println(colorGreen + "-------------------------------------------------------" + colorReset);
                    } // End of while loop
                    System.out.println(colorGreen + "=======================================================" + colorReset);
                    menu();
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                } // End of try-catch
                break;
        }
    } // End of menu method
} // End of TristanTest class





