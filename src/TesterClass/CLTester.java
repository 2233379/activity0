package TesterClass;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
public class CLTester {
    public static void main(String[] args) {
        String csvFile = "src/res/9443 Dataset.csv";
        List<InformationRequirements> dataset = readDataset(csvFile);

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("\n================================================");
            System.out.println("                     MENU                       ");
            System.out.println("================================================");
            System.out.println("1. Popular in general");
            System.out.println("2. Popular by genre");
            System.out.println("3. Popular by leading album");
            System.out.println("4. Popular by leading artist");
            System.out.println("5. Exit");
            System.out.println("================================================");
            System.out.print("Input Choice: ");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    displayOrderOptions(getPopularTracks(dataset), scanner);
                    break;
                case 2:
                    System.out.print("Enter the genre: ");
                    scanner.nextLine(); // Consume the newline character
                    String genre = scanner.nextLine();
                    displayOrderOptions(getPopularTracksByGenre(dataset, genre), scanner);
                    break;
                case 3:
                    displayOrderOptions(getPopularTracksByLeadingAlbum(dataset), scanner);
                    break;
                case 4:
                    displayOrderOptions(getPopularTracksByLeadingArtist(dataset), scanner);
                    break;
                case 5:
                    System.out.println("Exiting program...");
                    return;
                default:
                    System.out.println("Invalid choice. Please enter a valid option.");
            }
        }
    }
    /**
     * Applies the specified order to the given dataset.
     *
     * @param dataset   The dataset to be ordered.
     * @param ascending True for ascending order, false for descending order.
     * @return The ordered list of InformationRequirements.
     */
    private static List<InformationRequirements> applyOrder(List<InformationRequirements> dataset, boolean ascending) {
        List<InformationRequirements> orderedList = new ArrayList<>(dataset);
        if (ascending) {
            orderedList.sort(Comparator.comparingInt(track -> Integer.parseInt(track.getPopularity())));
        } else {
            orderedList.sort(Comparator.comparingInt(track -> Integer.parseInt(track.getPopularity())));
            Collections.reverse(orderedList);
        }
        return orderedList;
    }
    /**
     * Displays the ordering options and processes user input to display ordered tracks.
     *
     * @param tracks  The list of tracks to be ordered and displayed.
     * @param scanner Scanner object for user input.
     */
    private static void displayOrderOptions(List<InformationRequirements> tracks, Scanner scanner) {
        System.out.print("Choose order (1. Ascending, 2. Descending): ");
        int orderChoice = scanner.nextInt();
        boolean ascendingOrder = (orderChoice == 1);

        List<InformationRequirements> orderedTracks = applyOrder(tracks, ascendingOrder);
        displayPopularTracks(orderedTracks);
    }
    /**
     * Reads the dataset from a CSV file and creates a list of InformationRequirements objects.
     *
     * @param csvFile The path to the CSV file containing the dataset.
     * @return A list of InformationRequirements objects representing the dataset.
     */
    private static List<InformationRequirements> readDataset(String csvFile) {
        List<InformationRequirements> dataset = new ArrayList<>();

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(csvFile));
            String header = reader.readLine();

            String line;
            while ((line = reader.readLine()) != null) {
                String[] col;
                if (!line.contains("\"")) {
                    col = line.split(",");
                } else {
                    col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); //ignoring commas inside quotation marks
                }

                // Check if the array has the expected number of elements
                if (col.length >= 23) {
                    //InformationRequirements information = new InformationRequirements();

                    //dataset.add(information);
                } else {
                    // Log a warning or handle the case where the data is not as expected
                    System.out.println("Warning: Invalid data in CSV file. Skipping line.");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return dataset;
    }
    /**
     * Gets a list of popular tracks based on their overall popularity.
     *
     * @param dataset The dataset of InformationRequirements.
     * @return A list of popular tracks.
     */
    private static List<InformationRequirements> getPopularTracks(List<InformationRequirements> dataset) {
        dataset.sort(Comparator.comparingInt(track -> Integer.parseInt(track.getPopularity())));
        return dataset;
    }
    /**
     * Gets a list of popular tracks based on a specified genre.
     *
     * @param dataset The dataset of InformationRequirements.
     * @param genre   The genre to filter tracks.
     * @return A list of popular tracks within the specified genre.
     */
    private static List<InformationRequirements> getPopularTracksByGenre(List<InformationRequirements> dataset, String genre) {
        List<InformationRequirements> popularTracksByGenre = new ArrayList<>();
        for (InformationRequirements track : dataset) {
            if (track.getGenre().equalsIgnoreCase(genre)) {
                popularTracksByGenre.add(track);
            }
        }

        popularTracksByGenre.sort(Comparator.comparingInt(track -> Integer.parseInt(track.getPopularity())));
        return popularTracksByGenre;
    }
    /**
     * Gets a list of popular tracks from the leading album.
     *
     * @param dataset The dataset of InformationRequirements.
     * @return A list of popular tracks from the leading album.
     */
    private static List<InformationRequirements> getPopularTracksByLeadingAlbum(List<InformationRequirements> dataset) {
        InformationRequirements leadingAlbum = getLeadingAlbum(dataset);

        List<InformationRequirements> popularTracksByLeadingAlbum = new ArrayList<>();
        for (InformationRequirements track : dataset) {
            if (track.getAlbumID().equals(leadingAlbum.getAlbumID())) {
                popularTracksByLeadingAlbum.add(track);
            }
        }

        popularTracksByLeadingAlbum.sort(Comparator.comparingInt(track -> Integer.parseInt(track.getPopularity())));
        return popularTracksByLeadingAlbum;
    }
    /**
     * Gets a list of popular tracks from the leading artist.
     *
     * @param dataset The dataset of InformationRequirements.
     * @return A list of popular tracks from the leading artist.
     */
    private static List<InformationRequirements> getPopularTracksByLeadingArtist(List<InformationRequirements> dataset) {
        InformationRequirements leadingArtist = getLeadingArtist(dataset);

        List<InformationRequirements> popularTracksByLeadingArtist = new ArrayList<>();
        for (InformationRequirements track : dataset) {
            if (track.getArtistName().equals(leadingArtist.getArtistName())) {
                popularTracksByLeadingArtist.add(track);
            }
        }

        popularTracksByLeadingArtist.sort(Comparator.comparingInt(track -> Integer.parseInt(track.getPopularity())));
        return popularTracksByLeadingArtist;
    }
    /**
     * Gets the InformationRequirements object representing the leading album in the dataset.
     *
     * @param dataset The dataset of InformationRequirements.
     * @return The InformationRequirements object representing the leading album.
     */
    private static InformationRequirements getLeadingAlbum(List<InformationRequirements> dataset) {
        dataset.sort(Comparator.comparingInt(track -> Integer.parseInt(track.getPopularity())));
        return dataset.get(dataset.size() - 1);
    }
    /**
     * Gets the InformationRequirements object representing the leading artist in the dataset.
     *
     * @param dataset The dataset of InformationRequirements.
     * @return The InformationRequirements object representing the leading artist.
     */
    private static InformationRequirements getLeadingArtist(List<InformationRequirements> dataset) {
        dataset.sort(Comparator.comparingInt(track -> Integer.parseInt(track.getPopularity())));
        return dataset.get(dataset.size() - 1);
    }
    /**
     * Displays the details of the popular tracks.
     *
     * @param tracks The list of popular tracks to be displayed.
     */
    private static void displayPopularTracks(List<InformationRequirements> tracks) {
        System.out.println("Popular Tracks:");
        for (InformationRequirements track : tracks) {
            System.out.println("Track: " + track.getTrackName());
            System.out.println("Artist: " + track.getArtistName());
            System.out.println("Album: " + track.getAlbumID());
            System.out.println("Popularity: " + track.getPopularity());
            System.out.println("Genre: " + track.getGenre());
            System.out.println("===============================");
        }
    }}
