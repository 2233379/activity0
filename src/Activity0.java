import TesterClass.InformationRequirements;
import TesterClass.JanDaleTester;
import TesterClass.toSort;


import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;




/**
 * Activity 0


 * Tasks:
 * 1. Determine relevant information that can be provided through your data set. Think of
 * individual records as well as summarized / aggregated information that can be obtained
 * from the data set. It is expected that sorting and filtering options. Provide at least 5 such
 * information requirements.
 *
 * 2. Determine how to obtain the above information using a spreadsheet application such as
 * Excel / Google Spreadsheets.
 *
 * 3. Create a Java program (console-based program will do) that retrieves the information
 * described in #1 and displays the results
 */

public class Activity0 {
    static Scanner sc = new Scanner(System.in);
    static Activity0 activity0 = new Activity0();
    static Path csvFile = Path.of("src", "res/9443 Dataset.csv"); //initialized file path
    static String modifiedCsvFile =  "src/res/New File.csv";
    static String colorGreen = "\u001B[32m";
    static String colorYellow = "\u001B[33m";
    static String colorReset = "\u001B[0m";
    static String line; // Space
    int colNum;
    String element;


    public static void main(String[] args) {
        String origFile = "src/res/9443 Dataset.csv";
        String newFile = "src/res/New File.csv";
        removeDuplicates(origFile, newFile);
        activity0.menu();
        //activity0.listOfAristNames();
    }

    /**
     * Method to remove duplicate lines from a CSV file
     * @param origFile
     * @param newFile
     */
    public static void removeDuplicates(String origFile, String newFile) {
        Set<String> rows = new LinkedHashSet<>();


        try(BufferedReader br = new BufferedReader(new FileReader(origFile))) {
            String line;


            while ((line = br.readLine()) != null) {
                rows.add(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        try (BufferedWriter bw = new BufferedWriter(new FileWriter(newFile))){
            for (String row : rows) {
                bw.write(row);
                bw.newLine();
            }


        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Method for displaying the menu and handle user selections
     */
    public void menu() {

        String csvFile = "src/res/9443 Dataset.csv";
        List<InformationRequirements> dataset = readDataset(csvFile);
        boolean exit = false;
        while (!exit) {
            try {
                System.out.println(colorGreen + "\n============================================================================================" + colorReset);
                System.out.printf("%1s%45s%1s", colorYellow, "MENU ", colorReset);
                System.out.println(colorGreen + "\n============================================================================================" + colorReset);
                System.out.println("1. Frequency\n2. Correlation \n3. Average \n4. Minimum and Maximum \n5. Percentage  \n6. Exit ");
                System.out.println(colorGreen + "============================================================================================" + colorReset);
                System.out.print(colorYellow + "Please select a function: " + colorReset);
                int response = sc.nextInt();
                ;
                switch (response) {
                    case 1:
                        while (true) {
                            try {
                                System.out.println(colorGreen + "\n============================================================================================" + colorReset);
                                System.out.println("1. Artist Frequency\n2. Genre Frequency\n3. Key Frequency\n" +
                                        "4. Mode Frequency\n5. Return to Menu");
                                System.out.println(colorGreen + "============================================================================================" + colorReset);
                                System.out.print(colorYellow + "Select a Field: " + colorReset);
                                int field2 = sc.nextInt();
                                int columnIndex = 0;
                                switch (field2) {
                                    case 1:
                                        columnIndex = 3;
                                        frequency(columnIndex);
                                        break;
                                    case 2:
                                        columnIndex = 23;
                                        frequency(columnIndex);
                                        break;
                                    case 3:
                                        System.out.println("C Major = 0 \nG Major = 1 \nD Major = 2\nA Major = 3" +
                                                "\nE Major = 4 \nB Major = 5\nF# Major = 6\nC# Major = 7" +
                                                "\nG# Major = 8\nD# Major = 9\nA# Major = 10nE# Major = 11");
                                        columnIndex = 10;
                                        frequency(columnIndex);
                                        break;
                                    case 4:
                                        System.out.println("0 = Major\n 1 = Minor");
                                        columnIndex = 12;
                                        frequency(columnIndex);
                                        break;
                                    case 5:
                                        backToMenu();
                                        break;
                                    default:
                                        System.out.println("Invalid choice. Please enter a valid option.");
                                }
                                break; // Break out of the loop if input is valid
                            } catch (InputMismatchException e) {
                                System.out.println("Invalid input! Please enter a valid number.");
                                sc.nextLine(); // Clear the input buffer
                            } catch (Exception e) {
                                System.out.println("An error occurred: " + e.getMessage());
                            }
                        }
                        break;
                    case 2:
                        while (true) {
                            try {
                                System.out.println(colorGreen + "\n============================================================================================" + colorReset);
                                System.out.println("1. See Correlation \n2. Return to Menu");
                                System.out.println(colorGreen + "============================================================================================" + colorReset);
                                System.out.print(colorYellow + "Select a Function: " + colorReset);
                                int field3 = sc.nextInt();
                                switch (field3) {
                                    case 1:
                                        System.out.println("Danceability");
                                        int column = 8;
                                        String variableX = "danceability";
                                        correlation(column, variableX);


                                        System.out.println("Energy");
                                        column = 9;
                                        variableX = "energy";
                                        correlation(column, variableX);


                                        System.out.println("Key");
                                        column = 10;
                                        variableX = "key";
                                        correlation(column, variableX);


                                        System.out.println("Loudness");
                                        column = 11;
                                        variableX = "loudness";
                                        correlation(column, variableX);


                                        System.out.println("Mode");
                                        column = 12;
                                        variableX = "mode";
                                        correlation(column, variableX);


                                        System.out.println("Speechiness");
                                        column = 13;
                                        variableX = "speechiness";
                                        correlation(column, variableX);


                                        System.out.println("Acousticness");
                                        column = 14;
                                        variableX = "acousticness";
                                        correlation(column, variableX);


                                        System.out.println("Instrumentalness");
                                        column = 15;
                                        variableX = "instrumentalness";
                                        correlation(column, variableX);


                                        System.out.println("Liveness");
                                        column = 16;
                                        variableX = "liveness";
                                        correlation(column, variableX);


                                        System.out.println("Valence");
                                        column = 17;
                                        variableX = "valence";
                                        correlation(column, variableX);


                                        System.out.println("Tempo");
                                        column = 18;
                                        variableX = "tempo";
                                        correlation(column, variableX);
                                        backToMenu();
                                        break;
                                    case 2:
                                        backToMenu();
                                        break;
                                    default:
                                        System.out.println("Invalid choice. Please enter a valid option.");
                                }
                                break; // Break out of the loop if input is valid
                            } catch (InputMismatchException e) {
                                System.out.println("Invalid input! Please enter a valid number.");
                                sc.nextLine(); // Clear the input buffer
                            } catch (Exception e) {
                                System.out.println("An error occurred: " + e.getMessage());
                            }
                        }
                        break;
                    case 3:
                        Scanner scanner = new Scanner(System.in);
                        System.out.println("\n\n=====================================================================================================================================");
                        System.out.println("                                                    AVERAGE ELEMENT VALUES OF SONGS                                                       "); // change title
                        System.out.println("                                                 Please pick an artist of your choice                                                ");
                        System.out.println("=====================================================================================================================================");
                        String[] artists = {".swimwhere", "$NOT", "$uicideboy$", "$wave", "03 Greedo",
                                "1096 Gang", "10k.Caash", "1mill Records", "1nonly", "1way Frank",
                                "24hrs", "24kGoldn", "2CRE8", "300yearsyoung", "347aidan",
                                "3irty", "3xbravo", "4LilPat3", "5 Seconds of Summer", "50 Cent",
                                "6ix9ine", "7ru7h", "870glizzy", "88rising", "89ine",
                                "A Boogie Wit da Hoodie", "A-FRAME", "A$AP Ferg", "A1 x J1", "A5H",
                                "Aarika", "Aaron Smith", "Aaryan Shah", "Abaddon", "Absofacto",
                                "ABSRDST", "Ace Poetik", "Adam Aronson", "Adam Bü", "Adam Kahati",
                                "Addison Rae", "Adele", "Adso Alejandro", "Aegis", "Aesth",
                                "Aests", "Aexcit", "Aieysha Haws", "Aikee", "Åírös",
                                "AJ Gravity", "AJ Salvatore", "Ajay Stephens", "AJR", "Akano",
                                "Akon", "Al James", "Al Moralde", "Alan Walker", "Aldo Bz",
                                "Alessia Cara", "Alesso", "Alex Alexander", "Alex Aster", "Alex D'Rosso",
                                "Alex Edmonds", "Alex Gonzaga", "Alex McArtor", "Alex Megane", "Alex Rose",
                                "Alexa Ayaz", "Alexander 23", "Alexandra Stan", "Alfons", "Ali Gatie",
                                "Alina Baraz", "All Time Low", "Alle Farben", "ALLMO$T", "Allocai",
                                "Alok", "alt-J", "ALVIDO", "Alvin & The Chipmunks", "Aly & AJ",
                                "Amaru Cloud", "Amaru Son", "Ambjaay", "American Authors", "Aminé",
                                "Ana Shine", "Anamanaguchi", "Anderson .Paak", "Andre Swilley", "Andrew E.",
                                "Andrew Gold", "Andrew Spencer", "Andy Grammer", "AnimeHub", "Anitta",
                                "Anna Hamilton", "Anna Helen", "Anne-Marie", "Anoyd", "Anson Seabra",
                                "Ant Saunders", "Anthem Hits Platinum", "Anthony Gonzalez", "Anthony Keyrouz", "Anthony Q.",
                                "AP3", "Apekatten", "Apollo Fresh", "Apontay", "Aqyila",
                                "Arctic Monkeys", "ARIA", "Aria X", "Ariana Grande", "Arizona Zervas",
                                "Ark Patrol", "Armin van Buuren", "ARRIE", "Arts", "Arvey",
                                "Ashe", "Ashley O", "Ashley Price", "Ashley Tha MC", "Ashnikko",
                                "Asin", "Astrid S", "Astrokidjay", "Astrus*", "ATB",
                                "Au/Ra", "Audrey Mika", "August Kid", "Auntie Hammy", "AURORA",
                                "Austin Mahone", "Ava Max", "Avenue Beat", "Avi", "Avicii",
                                "Avocuddle", "Avril Lavigne", "awfultune", "Axel Black & White", "Aya Nakamura",
                                "AYO TR3", "Azealia Banks", "Azide", "Azjah", "B Free TMT",
                                "B. Smyth", "B.o.B", "Baby Bash", "Baby Keem", "BabyJake",
                                "Bad Bunny", "Badda TD", "Bag Raiders", "Bailey May", "Bandang Lapis",
                                "Bankrol Hayden", "BANNERS", "Banx & Ranx", "Barrett Crake", "Basgilano",
                                "Bassjackers", "Bay-C", "Bazzi", "bbno$", "BBY KODIE",
                                "Bea Miller", "Beach Bunny", "Beach House", "Beachbag", "Beanz",
                                "BeatKing", "Bebe Rexha", "Because", "Becky G", "Bee Gees",
                                "bee$", "Behind The Frames", "Bell Biv DeVoe", "Bella Poarch", "Ben&Ben",
                                "BENEE", "Benny Benassi", "benny blanco", "Benzi", "Besomorph",
                                "Beyoncé", "Bhad Bhabie", "Bhaskar", "BIA", "Big Homie Ty.Ni",
                                "Big Sean", "Big Wild", "BIGBANG", "Bigga Don", "BigKlit",
                                "Billie Eilish", "Billy Crawford", "Billy Joel", "Billy Marchiafava", "BIMONTE",
                                "Bishop Briggs", "Bizarrap", "Bizzy Crook", "Blac Youngsta", "black a.m.",
                                "Black Eyed Peas", "blackbear", "BlackMayo", "BLACKPINK", "Blaikz",
                                "Blaine", "Blanco", "Blanco Brown", "BLIND.SEE", "Blinded Hearts",
                                "Blondes", "bloom", "Blu DeTiger", "BLUE ENCOUNT", "Blue Jeans",
                                "Blueface", "BLUPRNT", "Blxst", "BLYMLDT", "BMW KENNY",
                                "Bob Marley & The Wailers", "Bobby Shmurda", "Bodybangers", "BODYWORX", "Boehm",
                                "Bonde R300", "Boney M.", "Bonnie Bailey", "Bop King", "Boring Lauren",
                                "BØRNS", "Bosx1ne", "Boza", "Braaten", "Braaten & Chrit Leaf",
                                "Brain Music", "Brando", "Brandon ThaKidd", "Brandy", "Brandz",
                                "Breakbot", "BreaksManado", "Brent Faiyaz", "Brickboydior", "Bridgit Mendler",
                                "Bring Me The Horizon", "Britney Spears", "BROCKHAMPTON", "BRONSON", "Brooks",
                                "Brown Skies", "BRS Kash", "Bruno Mars", "BrxkenBxy", "Bryson Tiller",
                                "BTS", "Bugoy Drilon", "bülow", "Burgos", "BURNOUT SYNDROMES",
                                "Busta Rhymes", "BYNX", "C Guapo", "C.LACY", "C2d",
                                "Cafe Disko", "Cage The Elephant", "CakeMan", "Calabasas", "Calboy",
                                "Cali Swag District", "Callista Clark", "Cally Rhodes", "Calmani & Grey", "Calum Scott",
                                "Calum Venice", "Calvin Harris", "CALVO", "Cameron Sanderson", "Camila Cabello",
                                "Camilo", "Cap Carter", "Capital Cities", "Cardi B", "Carla",
                                "Carlie Hanson", "Carly Rae Jepsen", "carolesdaughter", "Caroline Kole", "CARSTN",
                                "Cartoons", "Cash Koo", "CashMoneyAp", "Cassie", "Cast - Sofia the First",
                                "Caterina Valente", "Cdot Honcho", "Celeste Legaspi", "CELINE", "CG5",
                                "Chaël", "Championxiii", "Chance the Rapper", "Charli XCX", "Charlie Sloth",
                                "Charly Black", "Chase Atlantic", "CHASE B", "Cheflodeezy", "Chema Rivas",
                                "Chester Young", "Chico Rose", "Chiiild", "Children Under the Sun", "Chimbala",
                                "Chip Tha Ripper", "Chloe George", "Chloe x Halle", "Chord Overstreet", "Chris Brown",
                                "Chris Lawyer", "Chrishan", "Christian French", "Christina Perri", "CHROMANCE",
                                "Chuku100", "Chumino", "Chvnge Up", "Ciara", "Cico P",
                                "Cigarettes After Sex", "Cinderella", "cinnamons", "City Girls", "City James",
                                "CJ", "Claire Rosinkranz", "Clairo", "Claptone", "Claud Musik Indonesia",
                                "Clean Bandit", "CLR", "CMTEN", "Cobra Starship", "Cochise",
                                "Coco Lense", "Cody Simpson", "Coi Leray", "Coldplay", "Colone",
                                "Color Theory", "Comethazine", "Compton Ro2co", "Conan Gray", "Conkarah",
                                "Cookiee Kawaii", "Coolio", "Coopex", "Cordae", "Corinne Bailey Rae",
                                "CORPSE", "Cosculluela", "Cosmo Sheldrake", "Courtney", "Cousin Stizz",
                                "Coyote Theory", "Cozy Soundz", "CPX", "Craig David", "CRASH RARRI",
                                "Crazy Frog", "Crisaunt", "CRISPIE", "Crissin", "Crystal Rock",
                                "Cue C", "Cuebrick", "Cults", "Curtis Roach", "Curtis Waters",
                                "Cuuhraig", "Cytus ll", "D-Rah", "D12", "D4L",
                                "DaBaby", "Daddy Yankee", "Daddyphatsnaps", "Daft Punk", "Dagny",
                                "Dame Dame", "Damien", "Dan + Shay", "DancingRoom", "Daniel Allan",
                                "DankMemez", "Danko", "DankTiks", "Danny Aro", "Danny Avila",
                                "Danny Joseph", "Dante", "Dante Boy", "Dante Klein", "Danz Dow",
                                "DARKMARK", "Darling Brando", "Darwin", "Dastic", "Dathan",
                                "Dave East", "DAVESTATEOFMIND", "Davey Langit", "David Guetta", "David Puentez",
                                "David Shane", "David Shawty", "Dawin", "Daya Luz", "Dayme y El High",
                                "Daz Payne", "DAZZ", "DDG", "deadman 死人", "Dean Lewis",
                                "DeathbyRomy", "December Avenue", "Declan McKenna", "Deep Chills", "Deestroying",
                                "DeeWunn", "DeJ Loaf", "Dem Franchize Boyz", "Dem Rap Bolz", "Demi Lovato",
                                "Denari", "Denaron", "DenivMasadaMusic", "DENNIS", "Dermot Kennedy",
                                "Devault", "dewolS Music", "Dexys Midnight Runners", "Didrick", "Die Antwoord",
                                "Digga D", "Digrasso", "Dillon Francis", "Dimitri Vegas & Like Mike", "Dino Warriors",
                                "Diplo", "Dirty Heads", "DIRTYXAN", "Disclosure", "Disco Fries",
                                "Disco Lines", "Discofields", "Dixie", "Dixson Waz", "Diys",
                                "dj 6rb", "DJ ARTHUZIIN", "DJ Bianda", "DJ Bryanflow", "DJ Challenge X",
                                "DJ Chose", "DJ Cleitinho", "DJ Covy", "DJ Desa", "DJ DOLA do TRI NGULO",
                                "Dj Douyin Remix", "DJ Drobitussin", "Dj EddyBeatz", "DJ Eezy", "DJ Enjel",
                                "DJ Facemask", "DJ Gollum", "DJ Gotta", "DJ GRZS", "Dj Guuga",
                                "DJ Haning", "Dj Imut", "Dj Jac", "Dj Kass", "DJ Khaled",
                                "DJ Loonyo", "DJ Luc14no Antileo", "DJ Lucas Beat", "DJ Menor 7 DJ Menor da Dz7", "DJ Molasses",
                                "DJ Nanda", "Dj Nbeat", "DJ Noiz", "DJ Ofreck", "DJ Opus",
                                "DJ P13", "DJ Patrick Muniz", "Dj Peligro", "DJ Pro Code", "DJ Purpberry",
                                "DJ Qhelfin", "DJ Quarantine", "Dj Raulito", "DJ Record", "DJ Red Core",
                                "DJ Remix", "DJ Rowel", "Dj Rowel", "DJ Sauna", "DJ Sephora",
                                "Dj Serpinha", "Dj Setia", "dj Shawny", "DJ Siul", "Dj Smith Casma",
                                "DJ Souza Original", "DJ TikTok", "Dj Tiktokker", "DJ Viral", "Dj Vm",
                                "Dj W-Beatz", "Dj Wesley Gonzaga", "DJ Wrekshop", "DJ Xavierj713", "Dj Yaksy",
                                "Dj πrata", "DjDanz Remix", "DjKeinth", "DMNDS", "DNT RYE",
                                "Doja Cat", "Dolo Tonight", "DOM SHWN", "Don Diablo", "Don Omar",
                                "Don Toliver", "Donnalyn", "Drake", "Dramos", "Dre Banks",
                                "Dre'es", "Drew", "Drinche", "DripReport", "Dro Kenji",
                                "Dror", "Dua Lipa", "Dualities", "Dubdogz", "Dubskie",
                                "Duce Mino", "Dugem Nation", "Duncan Laurence", "DUSTY LOCANE", "Dustystaytrue",
                                "DVBBS", "Dyllón Burnside", "E the profit", "E-40", "Early Studios",
                                "Eastblock Bitches", "Eazy-E", "Ebony", "Ed Helms", "Ed Sheeran",
                                "Eddie Vedder", "EDEN", "Edith Whiskers", "Eduardo Luzquiños", "Edward Sharpe & The Magnetic Zeros",
                                "EGOVERT", "Ekoh", "El Alfa", "EL Fresh", "El Memer",
                                "El Nikko DJ", "El Papi", "El Villanord", "Elevating Sounds", "eli.",
                                "elie curtis", "Elilluminari", "Elizabeth Wyld", "Ella Eyre", "Ella Mai",
                                "Ella McCready", "ellee ven", "Ellie Goulding", "Eltee", "ElyOtto",
                                "Em", "Eminem", "Emman", "Emotional Oranges", "Engelwood",
                                "Enviousofenvy", "Eric Bellinger", "Erica Banks", "Eritza", "Estelle",
                                "ETHN ASHR", "Etta James", "Ev0lution", "Eva Grace", "Eve",
                                "Ex Battalion", "Eyedress", "Ez Mil", "EZRA", "F. Physical",
                                "F3DE", "Facemask Time", "Fairview", "Fallen Roses", "Famous Dex",
                                "Far East Movement", "Farruko", "Faruk Orman", "Fat Joe", "Febri Hands",
                                "Fedd the God", "FEL!X", "Felicia Lu", "Felix Samuel", "Felix Schorn",
                                "Felly", "Fendii AP", "Fergie", "Fetty Wap", "FiloChill",
                                "Finn Askew", "Fire Jane", "Flapjax", "Fleetwood Mac", "FLETCHER",
                                "Flo Milli", "Flo Rida", "flora cash", "Flow G", "Flow La Movie",
                                "Floxyn", "FNF Chop", "Foolio", "Fools Garden", "Forrest.",
                                "Foster", "Foster The People", "Fousheé", "FR!ES", "Frances Forever",
                                "Frank Ocean", "FRED PANOPIO", "Freddie Dredd", "Freddy Verano", "Freischwimmer",
                                "FreqLoad", "FreshDuzIt", "Freshie", "Freya Ridings", "Frostydasnowmann",
                                "FRVRFRIDAY", "FSDW", "Fugees", "Funzo & Baby Loud", "Future",
                                "G-Eazy", "Gabi Riveros", "Gabry Ponte", "Gagong Rapper", "Gaho",
                                "Galantis", "Galwaro", "Gamma1", "GAMPER & DADONI", "Gary Valenciano",
                                "Gaten Matarazzo", "GATTÜSO", "Gente De Zona", "Geo Ong", "George Michael",
                                "Gera MX", "Getter Jaani", "Giang Pham", "Gigi D'Agostino", "Gill the ILL",
                                "Gimme 5", "GIRLI", "Giuseppe Vittoria", "Giveon", "Gjesti",
                                "Glaceo", "Glady's and the Boxers", "Glass Animals", "Glee Cast", "Global Dan",
                                "Gloc 9", "Glockboyz Teejaee", "Gloria Estefan", "GoldLink", "Good Gas",
                                "Goodboys", "Gooney Jib", "Gorillaz", "Gotye", "Griegz",
                                "Gritty Lex", "Grouplove", "Grover Washington, Jr.", "Gryffin", "Gunna",
                                "Gustixa", "Guthrie Nikolao", "Guy Sebastian", "Guyon Waton", "Gwen Stefani",
                                "Gym Class Heroes", "H.E.R.", "Haise", "Haiti Babii", "Hale",
                                "Halsey", "Handsome Luke", "Hard Lights", "Harmless", "Harris & Ford",
                                "Harry Styles", "Haska", "HÄWK", "Hayd", "Hazel Faith",
                                "HBz", "Hd4president", "Heartbreak Aris", "Hedley", "Henny Seear",
                                "Henri Purnell", "Henry Hood", "HenryDaher", "Henyong Makata", "Herman",
                                "Heron & Serum", "Hey Joe Show", "Hey Santana", "High Quality The Label LLC", "highonclouds",
                                "Highup", "Hiko", "Hip Hop Harry", "Hippie Sabotage", "HL Wave",
                                "Hobo Johnson", "Hollaphonic", "Honcho Moonk", "Honey Gee", "HONNE",
                                "Hoobastank", "Hoodie Mitch", "Hotdog", "Hoxtones", "Hr. Troels",
                                "HRVY", "Hudson East", "Huey V", "HUGEL", "Huncho Da Rockstar",
                                "HUTS", "HVME", "Hwa Sa", "HXLLYWOOD", "HYO",
                                "Hyperclap", "I Belong to the Zoo", "I Don't Speak French", "I.U", "Iamdoechii",
                                "IAMJOSHSTONE", "iann dior", "Icona Pop", "Iggy Azalea", "Ijiboy",
                                "ilham", "ILIRA", "Ilkan Gunuc", "Ilkay Sencan", "Ill Snek",
                                "iLL Wayno", "Illijah", "iLOVEFRiDAY", "iLoveMemphis", "Imagine Dragons",
                                "Imago", "Imanbek", "Imran Khan", "imy2", "Inigo Pascual",
                                "INOJ", "Insane Clown Posse", "Internet Money", "Ir Sais", "Israel Del Amo",
                                "Issam Alnajjar", "ItsLee", "ITZY", "Iwansteep Official", "Iyaz",
                                "J Balvin", "J Boog", "J Swey", "J!mmy", "J. Cole",
                                "J.I the Prince of N.Y", "Jack & James", "Jack Be", "Jack Daily", "Jack Harlow",
                                "Jack Johnson", "Jack Stauber", "JACKBOYS", "Jacob Tillberg", "JACØBS",
                                "Jacquees", "JADE MOSS", "Jaden", "Jae Trill", "Jamayne",
                                "James Arthur", "James Bay", "James Blake", "James Carter", "James TW",
                                "Janine Teñoso", "Jannis Block", "Japanic", "Jasiah", "Jason Derulo",
                                "Jastefy", "Jawsh 685", "Jax Jones", "Jay Hoodie", "Jay Rock",
                                "Jay Sean", "Jay Wheeler", "JAY-Z", "Jazmin Sisters", "JBK",
                                "Je", "Jean Juan", "Jeaux", "Jelly Drops", "Jemme",
                                "JenCee", "Jenil", "Jenny Jewel", "Jensi Jenno", "jeonghyeon",
                                "Jeremih", "Jeremy Zucker", "Jerome", "Jersey Club", "Jess Benko",
                                "Jessi", "JESSIA", "Jessica Hammond", "Jessie J", "Jethro",
                                "Jewelz & Sparks", "Jhené Aiko", "Jhonni Blaze", "Jiaani", "JM Bales",
                                "Joel Corry", "JoeVille", "Joey + Kelly Thompson", "Joey Melrose", "Joey Purp",
                                "John Lennon", "John Mayer", "John Roa", "John The Blind", "John Williams",
                                "Johnnie Mikel", "Johnny Orlando", "Join The Club", "Joji", "JoJo",
                                "Jom", "Jon + Larsen", "Jon Eidson", "Joni Remix", "Jonn Hart",
                                "Jordan Jay", "Jose De Las Heras", "Josefine", "Joseph Black", "Josh A",
                                "Josiane Lessard", "Jowell & Randy", "Joy", "JP Saxe", "Jr Crown",
                                "JRL", "Juan Caoile", "juan karlos", "Jubël", "Juice WRLD",
                                "Julia Michaels", "Julieta Venegas", "June3rd", "Junie", "jusLo",
                                "Just Hush", "Just Ice", "Justin Bieber", "Justin Morelli", "Justin Quiles",
                                "Justin Timberlake", "Justin Vasquez", "Justin Wellington", "Justina Valentine", "JustRap",
                                "JVKE", "JVLA", "jxdn", "Jyxo", "K And The Boxers",
                                "K CAMP", "K-391", "K.A.A.N.", "K.Moses", "K'ron",
                                "KA!RO", "Kaash Paige", "Kaayne", "KAAZE", "Kaito Shoma",
                                "Kakaiboys", "Kali", "Kali Cass", "Kali Uchis", "Kamaiyah",
                                "Kamil", "KANA-BOON", "Kane Brown", "Kanye West", "Kaphy",
                                "Kapthenpurek", "Kardinal Offishall", "Karencitta", "Karl Wine", "KAROL G",
                                "Karri", "Kash Doll", "Kat Meoz", "Kat Nova", "Katy Perry",
                                "Katya Mila", "Kavale", "Kayla Nicole", "KAYTRANADA", "KAYY ELL",
                                "KBFR", "KD One", "Kelis", "Kendra Erika", "Kendrick Lamar",
                                "Kenjhons", "Kenndog", "Kensuke Ushio", "Kero Kero Bonito", "Kerry Wheeler",
                                "Kesh Kesh", "Kesha", "keshi", "Kevin Cartoon", "Kevin Courtois",
                                "Kevin Gates", "Kevin Powers", "Keyshia Cole", "Khia", "Kid Alina",
                                "Kid Cudi", "Kid Francescoli", "Kid Ink", "Kid Travis", "Kidd Keo",
                                "KIDS SEE GHOSTS", "KIIINGSAM", "Kikuo", "KillBunk", "KILLY",
                                "Kim Chiu", "Kim Dracula", "Kina", "King Badger", "King Combs",
                                "King Critical", "King Kaybee", "King Princess", "King Staccz", "King Tutt",
                                "King Von", "KINGMOSTWANTED", "kingpuntocom beats", "Kings of Leon", "Kinneret",
                                "Kir", "Kira P", "Kito", "Kitt Wakeley", "Kiubbah Malon",
                                "Kiyo", "Kizz Daniel", "Klaas", "KLANG", "Klaus Badelt",
                                "Klave", "Koda", "Kodak Black", "Kolohe Kai", "Kooly Bros",
                                "Koosen", "Kortnee Simmons", "kostromin", "Kota Banks", "Krazymut",
                                "Kreepa", "Kris Yute", "krisostomo", "KRYPTO9095", "KSHMR",
                                "KSI", "Kule Ka$h", "Kumarion", "Kungs", "Kuya Bryan",
                                "KVSH", "Kyan Palmer", "Kygo", "KYLE", "Kyle Echarri",
                                "Kyle Exum", "Kyle Juliano", "Kyle The Hooligan", "Kyle Zagado", "L.Dre",
                                "L'Trimm", "La Memerano", "La Nevula23 Productor", "LA Rodriguez", "La Roux",
                                "Labrinth", "Lady Gaga", "Ladytron", "Lakim", "Lana Del Rey",
                                "LANDR", "Landstrip Chip", "LANNÉ", "Lara Anderson", "Lara Silva",
                                "Larissa Lambert", "Larray", "Larusso", "Lary Over", "Latto",
                                "LAUWE", "Lavaado", "Lbdjs Record", "LBS Kee'vin", "Le Pedre",
                                "le Shuuk", "Le Tigre", "Leat'eq", "Leeric", "Lele Pons",
                                "Leo Santana", "Leon Jaar", "Leony", "Leska", "Lew Mr. Blue",
                                "Lewis Capaldi", "Lexi Jayde", "Liam Payne", "Life Loops", "Lifs_a_Trip",
                                "Likybo", "Lil Baby", "Lil Boom", "Lil Cray", "Lil Darkie",
                                "Lil Dicky", "Lil Flop", "Lil Heavy Chainz", "Lil Jon", "Lil Kapow",
                                "Lil Kayla", "Lil Keed", "LIL MAYO", "Lil Memer", "Lil Mosey",
                                "Lil Nas X", "LIL NIX", "Lil Peep", "Lil Perfect", "Lil Rekk",
                                "Lil Scrappy", "Lil Shock", "Lil Sil", "Lil Tecca", "Lil Tjay",
                                "Lil Toe", "Lil Uzi Vert", "Lil Vinceyy", "Lil Wayne", "Lil Wil",
                                "Lil Xxel", "Lil Yachty", "lilbootycall", "LILHUDDY", "Lilianna Wilde",
                                "lilspirit", "Lily Allen", "Limón Limón", "Lindsey Stirling", "Linked Horizon",
                                "Lion", "Lipless", "liquidfive", "LiSA", "Lit Killah",
                                "Little Mix", "Little Monstaz", "Little Simz", "LIZ", "LIZOT",
                                "Lizz Robinett", "Lizzo", "Lloyd", "LLusion", "Lofi Fruits Music",
                                "Lonely God", "Lonr.", "Loonie", "Lord Huron", "Lorde",
                                "Los Dioses Del Ritmo", "Los Farandulay", "Los Legendarios", "Lost People", "Lotus",
                                "Lou Bega", "Loui", "Louis Tomlinson", "Love Harder", "LPB Poody",
                                "LT", "Lucas & Steve", "Lucas Coly", "Lucas Estrada", "Lucio",
                                "Lucky Luke", "Ludacris", "Ludvigsson", "Luh Baam", "Luh Kel",
                                "Luki", "LUM!X", "LUNAX", "Lund", "LVP",
                                "Lykke Li", "Lyss", "M Fair", "M83", "Mabel",
                                "Mac Miller", "Macaulay Sulkin", "Machine Time", "Macklemore", "Macklemore & Ryan Lewis",
                                "Mad Dogz", "Madcon", "Madism", "Madison Beer", "Maejor",
                                "Maggie Lindemann", "Magnus Haven", "Mahogany Lox", "Mahout", "Maikitol",
                                "Majestic", "Major Lazer", "Mak Sauce", "Mala Agatha", "Malu",
                                "Maluma", "Mangoo", "MANILA GREY", "Marc Benjamin", "Marc E. Bassy",
                                "Marc Korn", "MarcLegend", "Marco Nobel", "Marcus & Martinus", "Marcus Layton",
                                "Mariah Carey", "MARINA", "Marion Aunor", "Mark B.", "Mark Carpio",
                                "Mark Mendy", "Maroon 5", "Mars Wagon", "Marshmello", "Martin Arteta",
                                "Martin Garrix", "Martin Jensen", "Martin van Lectro", "Marv Allen", "Marwa Loud",
                                "Mary Jane Girls", "Mary Mary", "Masatoshi Ono", "Masego", "Masked Wolf",
                                "MASN", "Masove", "Master KG", "Mathias.", "Mati Masildo",
                                "Matoma", "Matthaios", "Matthew Wilder", "Matvey Emerson", "Mauria",
                                "Màvcase", "Maxence Cyrin", "Maximillian", "Maximo", "MaxsIndBass",
                                "Maykel Mantow", "Mayonnaise", "Mazen", "MC 3L", "Mc Abalo",
                                "MC Cego Abusado", "MC CH da Z.O", "Mc Davi", "Mc Draak", "MC Einstein",
                                "Mc Kaio", "Mc Kevin", "MC Kevin o Chris", "Mc Lele", "Mc Livinho",
                                "MC Magrella", "MC Marley", "MC Matheuzinho", "MC Meno K", "MC MN",
                                "Mc Rennan", "MC Torugo", "MC Virgins", "Mc Zaac", "MC Zaquin",
                                "MC4D", "MEDUZA", "Mega Shinnosuke", "Megan Thee Stallion", "Meghan Trainor",
                                "Melanie Martinez", "Melonia", "MePemuro", "Merty Shango", "Meryl Streep",
                                "Mesto", "metr", "Metrixx", "Metro Boomin", "Metro Station",
                                "MGMT", "Mi Pan Ñam Ñam", "Micast", "Michael Bars", "Michael Dutchi Libranda",
                                "Michael Jackson", "Michael Pangilinan", "Midi Blosso", "Mighty Bay", "Migos",
                                "Migrantes", "Miguel", "Mike Candys", "Mike Dimes", "Mike La Funk",
                                "Mike Posner", "Mike Vallas", "Miley Cyrus", "Millie B", "Milly",
                                "mimi bay", "Mimi Webb", "mimiyuuuh", "Minerro", "Missy Elliott",
                                "Mitski", "Miura Jam", "MixAndMash", "MkX", "Mo2crazee",
                                "Mobbstarr", "Mobile Legends: Bang Bang", "Moguai", "Mohead Mike", "Mohombi",
                                "Moira Dela Torre", "MOKABY", "MOL$", "Molly Brazy", "Momokurotei Ichimon",
                                "Money Man", "Moneybagg Yo", "MoneyMarr", "Monica", "Monte Booker",
                                "Moon OA", "Moonstar88", "Mooski", "Mora", "More Fatter",
                                "MORGENSHTERN", "Morissette", "Mother Mother", "MOTi", "Mozzy",
                                "Mr_hotspot", "Mr. 2-17", "Mr. Budots", "Mr. C", "Mr. Pig",
                                "Mrs. GREEN APPLE", "Ms.OOJA", "Muffin", "Mulherin", "Mumford & Sons",
                                "Muse", "Music Hero", "Mustard", "Myke Towers", "Nadine Lustre",
                                "Nadson O Ferinha", "Naeleck", "Nakala", "Namelle", "Nanda Lia",
                                "Nardo Wick", "Natalie Taylor", "Natasha Mosley", "Nathan Evans", "Natio",
                                "Natti Natasha", "NaXwell", "Nazia Marwiana", "Ne-Yo", "Nea",
                                "Near", "Nelly", "Nelly Furtado", "Nene Malo", "Neoni",
                                "Neptunica", "Ness The Kid", "Netta", "NextYoungin", "NF",
                                "Nfasis", "Nick Cave & The Bad Seeds", "Nick Le Funk", "Nicki Minaj", "Nicky Romero",
                                "Nicole Chambers", "nicopop.", "Nightmare", "Niiko x SWAE", "Nik Makino",
                                "Niko B", "Nio Garcia", "Nippandab", "NLE Choppa", "No Genre",
                                "Noah Cyrus", "Noble Lyfe", "Nodis", "Nofin Asia", "Noisettes",
                                "Nora Van Elken", "Number9ok", "NvMe", "O Masokista", "O.C. Dawgs",
                                "ocean", "Ocho Drippin", "Octavian", "Ofenbach", "offrami",
                                "Offset", "OG Gazo", "Okean Elzi", "OLA RUNT", "Oliver Heldens",
                                "Olivia Newton-John", "Olivia O'Brien", "Olivia Rodrigo", "Olympis", "Omarion",
                                "OMC", "Omega", "omgkirby", "OpenSoul", "Orange & Lemons",
                                "Osocali", "OT BEATZ", "Otavio Beats", "Outkast", "oxy_gin",
                                "Ozuna", "P!nk", "Papa Zeus", "Paper Idol", "papichuloteej",
                                "Papu DJ", "Parari", "Paris Boy", "Party Favor", "Pascal Letoublon",
                                "Past Present", "Pateezy", "PatrickReza", "Paul Anka", "Paul BornaStar",
                                "PDL", "Peach Tree Rascals", "Peachy!", "PEDRO", "PEDRO SAMPAIO",
                                "Peewee Longway", "Perfect Pitch", "Peter Kuli", "PETER LAKE", "Peter Manos",
                                "Pháo", "Phay", "Phil The Beat", "Phillip Phillips", "PhoMeme",
                                "Pia Mia", "Picco", "Pink Chanel Dior", "Pink Guy", "Pink Sweat$",
                                "Piso 21", "Pitbull", "PiuPiu", "Pixies", "Plain White T's",
                                "Plan B", "Playboi Carti", "Pleasure P", "Plies", "Plustwo",
                                "PLVTINUM", "PmBata", "PnB Rock", "POCAH", "Polo & Pan",
                                "Polo Frost", "Polo G", "Pooh Shiesty", "Pop Smoke", "Popp Hunna",
                                "Porter Robinson", "Post Malone", "Power Beats Club", "Powfu", "ppcocaine",
                                "Precious Brown", "Priceless Da Roc", "Prince of Falls", "Prince Peezy & Lala Chanel", "Prince Royce",
                                "Princess Nokia", "Princess Thea", "Pronto Spazzout", "Prophet the Artist", "Psychedelic Boyz",
                                "Ptazeta", "PUBLIC", "Pulsedriver", "Puri", "Purple Disco Machine",
                                "Pusher", "Putih Abu-Abu", "Pyrex Pryce", "PYT. NY", "qtpie",
                                "Quality Control", "Quavo", "Que 9", "Que Fieri", "QUE.",
                                "Queen", "Qveen Herby", "R. Kelly", "R.I.O.", "R3HAB",
                                "Rachel Lorin", "Rachel Platten", "Radical Face", "RADWIMPS", "Rae Rae",
                                "Rae Sremmurd", "Raf Davis", "Rah Swish", "Rainych", "Rak-Su",
                                "Ramz", "Randarzky", "Rane Marie", "Rarin", "Rasmus Gozzi",
                                "Rasster", "Rat City", "Rauw Alejandro", "Rawi Djafar", "Ray J",
                                "Ray Le Fanue", "Ray Moon", "Rayface", "RC", "RC Blizz",
                                "Redbone", "Redfoo", "Regard", "Reggie Mills", "Reik",
                                "Remi Wolf", "Remix Kingz", "Rennan da Penha", "Reo Brothers", "reptilelegit",
                                "Revelries", "Rex Orange County", "REY VALERA", "Reyanna Maria", "Reyn Hartley",
                                "Rich Homie Quan", "Rich Music LTD", "Rich The Kid", "Rick James", "Ricky Desktop",
                                "Ricky Montgomery", "Rico Pressley", "Riff Raff", "Riflman", "Rihanna",
                                "Ringnes-Ronny", "Rio", "Riotron", "Riton", "Ritt Momney",
                                "Rivertree Music", "Rizky Ayuba", "Rizzy Rackz", "Roar", "Rob Deniel",
                                "Rob V", "Robaer", "Robbe", "Robbie Doherty", "Robbie Williams",
                                "Robert Vargas", "Robin M", "Robin Schulz", "Rochy RD", "Rod Wave",
                                "Roddy Ricch", "Roel Cortez", "Roji", "Rolipso", "Rolling Stone P",
                                "Roman Yasin", "Rompasso", "Ron Grams", "Ron Henley", "Rontae Don't Play",
                                "ROSALÍA", "Rot Ken", "Roundrobin", "Roxie", "RU$$OV",
                                "Ruan Nagel", "RudeBoyKels", "Run–D.M.C.", "RuskieBanana", "Russ",
                                "Ruth B.", "Rvssian", "Ryan Celsius Sounds", "Ryan Shade", "Rylo Rodriguez",
                                "Ryn Weaver", "S1mba", "Sabby Sousa", "Sad Puppy", "Sada Baby",
                                "Sadfriendd", "Sage The Gemini", "SahBabii", "SAINt JHN", "SAKVREYE",
                                "Salatiel", "salem ilese", "SALES", "Salt-N-Pepa", "salvia palth",
                                "Sam Bird", "Sam Concepcion", "Sam Feldt", "Sam Fischer", "Sam Smith",
                                "Sampaguita", "Sanjoy", "Sara Kays", "Sarah Almoril", "Sarah Geronimo",
                                "Sasha Saidin", "Saweetie", "SAYGRACE", "SB NewGen", "Scarlet Pleasure",
                                "Scarlett Taylor", "ScHoolboy Q", "Scooter", "Scotty", "Sean Finn",
                                "Sean Kingston", "Sean Paul", "SEB", "Sebastian Rhodes", "Sebastian Yatra",
                                "Sech", "Seeb", "Selena Gomez", "Selphius", "SelteMemset",
                                "Sem", "Semme", "Seniel Music", "Senior", "Serena Isioma",
                                "Sevyn Streeter", "Sezzy", "SGE Kash", "Shadow Cliq", "Shaggy",
                                "Shakira", "Shakka", "Shaky Vibes", "ShalomTeck", "Shanti Dope",
                                "SHAUN", "Shaun Reynolds", "Shawn Mendes", "Shayne Orok", "Sheck Wes",
                                "Shehyee", "Sheppard", "Sherwyn", "Shiko Nightcore", "Shou",
                                "Showtek", "Shumy Luke", "Shygirl", "Sia", "SICKOTOY",
                                "Sigurd Barrett", "Silent Child", "Silent Sanctuary", "Simple Plan", "SimxSantana",
                                "sinakun", "SINAN", "Sinoda", "Sir Chloe", "Sir Trilli",
                                "Sir Winz", "Sista Prod", "SISTAR19", "Ski Mask The Slump God", "Skusta Clee",
                                "Sky Kid", "Skylar Stecker", "SLANDER", "Slapaholics", "Sleepy Chows",
                                "Sleepy Hallow", "Slikk Eastwood", "Slim K", "Slowed Music", "SlowMemer",
                                "Smooth Vanilla", "Smugglaz", "Snakehips", "Snoop Dogg", "Social House",
                                "SoFaygo", "Soft Q", "Sohodolls", "Solstice", "SOMI",
                                "SOMMA", "Sondr", "Sonia Stein", "Sonya Belousova", "SoulChef",
                                "Soulja Boy", "Soulstice", "Soulthrll", "Soundofyou", "SpaceMan Zack",
                                "Speed Gang", "SPENCE", "SpotemGottem", "Ss.hh.a.n.a", "Star Music Artist",
                                "StarBoi3", "Starley", "Starz", "StaySolidRocky", "STEEL",
                                "Stef", "Steff da Campo", "Stefflon Don", "Stegosaurus Rex", "Stella Jang",
                                "Stellar", "Stephany Joanna", "Steve Aoki", "Steve Brian", "Steve Lacy",
                                "Steve Void", "Steven Universe", "Steven Young", "Stileto", "Stisema",
                                "Stonebwoy", "Strange Fruits Music", "Strawberry Guy", "Studio Killers", "Stunna Bam",
                                "Stunna Gambino", "Stunt DeGrate", "Stunt N Dozier", "Stupid Goldfish", "Sub Urban",
                                "SUD", "Sueco", "Sugar Jesus", "sumika", "Summer Walker",
                                "SummerDrives", "Super Yei", "Surf Curse", "Surf Mesa", "Surfaces",
                                "Svniivan", "SyKo", "SYML", "SZA", "T H R O N E",
                                "T-Pain", "T-Speed & 5upamanHOE", "T-Wayne", "t.A.T.u.", "T3",
                                "Tai Verdes", "Tainy", "Tame Impala", "Tamia", "Tate McRae",
                                "Tay Money", "Tay-K", "Taylor Swift", "Tayy Brown", "tearo, the ghost",
                                "Tedy", "Tekla", "TELYKast", "TEMPOREX", "Terelulopo",
                                "Terry Tesla", "Tesher", "Tessa Violet", "Tewy", "Th CDM",
                                "The Art Of Noise", "The Backyardigans", "The Band Perry", "The Boyfriends", "THE BOYZ",
                                "The Cardigans", "The Chainsmokers", "The Dance Queen Group", "The Execs", "The FifthGuys",
                                "The Future Kingz", "The Hollister Jean Lab", "The Juans", "The Kid LAROI", "The Living Tombstone",
                                "The Lumineers", "The Marimba Squad", "The Neighbourhood", "The Notorious B.I.G.", "The Paper Flowers",
                                "The Prince Karma", "The Pussycat Dolls", "The Remix Guys", "THE SCOTTS", "The Script",
                                "The Strokes", "The Strumbellas", "The Suncatchers", "The Underdog Project", "The Vamps",
                                "The Victor", "The Weeknd", "The Wombats", "Thiaguinho MT", "Thierry Von Der Warth",
                                "This Band", "thmpsn", "Thomas Reid", "Thundercat", "Thyro",
                                "Tia", "Tiagz", "Tierra Whack", "Tiësto", "TikTokCenter",
                                "Timbaland", "Timmy Thomas", "Timmy Trumpet", "Tinashe", "tinguspingus",
                                "Tiny Meat Gang", "Tion Wayne", "Tippy Dos Santos", "TisaKorean", "Tiscore",
                                "TIX", "TJ Monterde", "TK from Ling tosite sigure", "TK N Cash", "TMW",
                                "tobi lou", "Toby Fox", "TOD", "Todrick Hall", "TOKYO’S REVENGE",
                                "Tom Jones", "Tom Odell", "Tom Walker", "Tomcraft", "Tommee Profitt",
                                "Tones And I", "Tony Igy", "Tony Willrich", "TOO", "Top5",
                                "Topic", "TorontoMusicPlug", "Tory Lanez", "Tove Lo", "Tove Styrke",
                                "Trap Beckham", "trapwithjames", "Travis Porter", "Travis Scott", "Tre Coast",
                                "Tre Savage", "Trevor Daniel", "Trevor Wesley", "Trey Forever", "Trey Songz",
                                "Trill Ryan", "Trilly Trills", "Trippie Redd", "Trouze", "Troye Sivan",
                                "Tungevaag", "Tusse", "TV Girl", "TWICE", "Twista",
                                "Twisted Harmonies", "Two Feet", "Two Maloka", "Two Neighbors", "twocolors",
                                "TWOPILOTS", "Ty Dolla $ign", "Tyga", "Tyler April", "Tyler, The Creator",
                                "Tz da Coronel", "TZAR", "Ugly God", "UglyFace", "Ultradiox",
                                "Ummet Ozcan", "Unghetto Mathieu", "United Idol", "Up Dharma Down", "UPSAHL",
                                "Usher", "V.I.C.", "Valentino Khan", "VAMERO", "Vampire Weekend",
                                "Vanessa Carlton", "Vanfire", "Vaundy", "Vazer", "Vedo",
                                "Vegedream", "Vengaboys", "Venti", "VGR", "Vibe Street",
                                "Vicente Mejillano Jr", "Vicetone", "Victoria Monét", "VIEGO", "Vierre Cloud",
                                "VINAI", "Vinny West", "Vinoh", "Vion Konger", "Viral DJs",
                                "Visualz By Mariee", "Vito V", "Viva Hot Babes", "Viva La Panda", "VIZE",
                                "VMZ", "Volac", "VST & Company", "VVS Collective", "W&W",
                                "Wabi Sabi", "Waka Flocka Flame", "Wale", "Wallows", "wanderoof",
                                "Weezer", "Weirdo King", "West Monroe", "WHATEVER WE ARE", "Whigfield",
                                "WhiteCapMusic", "Whitney Houston", "WhoHeem", "Wight Kid", "Wiill",
                                "Will Joseph Cook", "Will Ryte", "Will Smith", "WilliamGregory", "WILLOW",
                                "Willy Anggawinata", "Wisin", "Wiz Khalifa", "WizKid", "Woodii",
                                "Wyatt Thunder", "X Ambassadors", "X Lovers", "XANAKIN SKYWOK", "Xand Avião",
                                "Xanity", "Xb Gang", "Xilo", "xotrapp", "XXXTENTACION",
                                "Y2K", "Yakusaaa", "Yandro El Protagonista", "Yannc", "yaveco",
                                "Yayoi", "YBN Nahmir", "Years & Years", "Yeng Constantino", "Yes",
                                "YFN Lucci", "YG", "Ying Yang Twins", "YK Osiris", "YN Jay",
                                "YNG Martyr", "YNW Melly", "Yo Gotti", "YOASOBI", "Yoh kamiyama",
                                "yoitsvic", "Yolanda Be Cool", "Yorii", "Yoshihisa Hirano", "Yot Club",
                                "Young B", "Young Fanatic", "Young Fresh", "Young M.A", "Young Maylay",
                                "Young Nudy", "Young T & Bugsey", "Young Thug", "Young Tone", "YoungBoy Never Broke Again",
                                "Younotus", "YSB Eli", "Ysl Jalen", "Yumi Lacsamana", "Yung Baby Tate",
                                "Yung Bae", "Yung Bleu", "Yung Gravy", "YUNG NATION", "Yung Nazty",
                                "Yung Pinch", "Yung Pooda", "Yung Raja", "Yung Skrrt", "Yung Yung Pallo",
                                "YUNGBLUD", "Yungeen Ace", "YungManny", "Yungster Jack", "Yves V",
                                "Z. Weina", "Zacai", "Zach Farache", "Zack Tabudlo", "ZaeHD & CEO",
                                "ZAYN", "Zedd", "Zephanie", "Zita", "Zoe Wees",
                                "Zombic", "Zum", "ギヴン", "クラピカ(CV:沢城みゆき)", "たかやん",
                                "ヒソカ(CV:浪川大輔)", "小潘潘", "物語シリーズ", "稲葉曇", "美波",
                                "音阙诗听"
                        };
                        int columns = 4;
                        int rows = (int) Math.ceil((double) artists.length / columns);
                        int maxLength = 0;
                        for (String artist : artists) {
                            maxLength = Math.max(maxLength, artist.length());
                        }


                        for (int i = 0; i < rows; i++) {
                            for (int j = 0; j < columns; j++) {
                                int index = i + j * rows;
                                if (index < artists.length) {
                                    String artist = artists[index];
                                    System.out.printf("%-" + (maxLength + 2) + "s", artist);
                                }
                            }
                            System.out.println();
                        }
                        System.out.println("-------------------------------------------------------------------------------------------------------------------------------------");
                        System.out.print(" Input Choice: ");
                        String choice = scanner.nextLine();

                        colNum = 8;
                        element = "danceability";
                        average(choice ,colNum, element);

                        colNum = 9;
                        element = "energy";
                        average(choice ,colNum, element);

                        colNum = 10;
                        element = "key";
                        average(choice ,colNum, element);

                        colNum = 11;
                        element = "loudness";
                        average(choice, colNum, element);

                        colNum = 12;
                        element = "mode";
                        average(choice, colNum, element);

                        colNum = 13;
                        element = "speechiness";
                        average(choice, colNum, element);

                        colNum = 14;
                        element = "acousticness";
                        average(choice, colNum, element);

                        colNum = 16;
                        element = "liveness";
                        average(choice, colNum, element);

                        colNum = 17;
                        element = "valence";
                        average(choice, colNum, element);

                        backToMenu();
                        break;
                    case 4:
                        int field5 = 0;
                        System.out.println(colorGreen + "\n============================================================================================" + colorReset);
                        System.out.println("1. Loudest and Quietest Songs\n2. Most and Least Popular Songs\n3. Highest and Lowest Speech rate Songs\n4. Most and Least Energetic Songs" +
                                "\n5. Most and Least Acoustic Songs \n6. Most And Least Instrumental Songs \n7. Most and Least Lively \n8. Highest and Lowest Valence Songs" +
                                "\n9. Highest and Lowest Tempo songs\n10. Return to Home");
                        System.out.println(colorGreen + "============================================================================================" + colorReset);
                        System.out.print(colorYellow + "Select a Field: " + colorReset);
                        do {
                            field5 = sc.nextInt();
                            if(field5 != 1 && field5 != 2 && field5 != 3 && field5 != 4 && field5 != 5 && field5 != 6
                                    && field5 != 7 && field5 != 8 && field5 != 9&& field5 != 10)
                                System.out.println("Enter a number between 1-12!");
                        }while (field5 != 1 && field5 != 2 && field5 != 3 && field5 != 4 && field5 != 5 && field5 != 6
                                && field5 != 7 && field5 != 8 && field5 != 9&& field5 != 10);

                        switch (field5) {
                            case 1:
                                getQuietestAndLoudestSongs();
                                backToMenu();
                                break;
                            case 2:
                                getMostAndLeastPopularSongs();
                                backToMenu();
                                break;
                            case 3:
                                getMostAndLeastSpeechinessSongs();
                                backToMenu();
                                break;
                            case 4:
                                getMostAndLeastEnergeticSongs();
                                backToMenu();
                                break;
                            case 5:
                                getMostAndLeastAcousticSongs();
                                backToMenu();
                                break;
                            case 6:
                                getMostAndLeastInstrumentalSongs();
                                backToMenu();
                                break;
                            case 7:
                                getMostAndLeastLivelySongs();
                                backToMenu();
                                break;
                            case 8:
                                getHighestAndLowestValenceSongs();
                                backToMenu();
                                break;
                            case 9:
                                getHighestAndLowestTempoSongs();
                                backToMenu();
                                break;
                            case 10:
                                break;
                        }//end of inner switch
                        break;
                    case 5:
                        while (true) {
                            try {
                                System.out.println(colorGreen + "\n============================================================================================" + colorReset);
                                System.out.println("1. Artist Percentage\n2. Genre Percentage\n3. Key Percentage\n4. Mode Percentage\n5. Return to Menu");
                                System.out.println(colorGreen + "============================================================================================" + colorReset);
                                System.out.print(colorYellow + "Select a Field: " + colorReset);
                                int field6 = sc.nextInt();
                                int columnIndex = 0;
                                switch (field6) {
                                    case 1:
                                        columnIndex = 3;
                                        percentageSolving(columnIndex);
                                        break;
                                    case 2:
                                        columnIndex = 23;
                                        percentageSolving(columnIndex);
                                        break;
                                    case 3:
                                        System.out.println("C Major = 0 \nG Major = 1 \nD Major = 2\nA Major = 3" +
                                                "\nE Major = 4 \nB Major = 5\nF# Major = 6\nC# Major = 7" +
                                                "\nG# Major = 8\nD# Major = 9\nA# Major = 10nE# Major = 11");
                                        columnIndex = 10;
                                        percentageSolving(columnIndex);
                                        break;
                                    case 4:
                                        System.out.println("0 = Major\n1 = Minor");
                                        columnIndex = 12;
                                        percentageSolving(columnIndex);
                                        break;
                                    case 5:
                                        backToMenu();
                                        break;
                                    default:
                                        System.out.println("Invalid choice. Please enter a valid option.");
                                }
                                break; // Break out of the loop if input is valid
                            } catch (InputMismatchException e) {
                                System.out.println("Invalid input! Please enter a valid number.");
                                sc.nextLine(); // Clear the input buffer
                            } catch (Exception e) {
                                System.out.println("An error occurred: " + e.getMessage());
                            }
                        }
                        break;
                    case 6:
                        System.out.println("Thank you for using the program!");
                        System.exit(0);
                    default:
                        System.out.println("Please select a valid option!!");
                }

            } catch (InputMismatchException e) {
                System.out.println("Invalid input! Please enter a valid number.");
                sc.nextLine();
            } catch (Exception e) {
                System.out.println("An error occurred: " + e.getMessage());
            }
        }
    }


    public void filterByGenre() {
        String filePath = "src/res/9443 Dataset.csv";
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\n========================");
        System.out.println("     SONGS BY GENRE    ");
        System.out.println("========================");
        System.out.println(" 1. _TIKTOK");
        System.out.println(" 2. TIKTOK DANCE");
        System.out.println(" 3. TIKTOK OPM");
        System.out.println(" 4. TIKTOK PHILIPPINES");
        System.out.println("------------------------");
        System.out.print(" Input Choice: ");
        int choice = Integer.parseInt(scanner.nextLine());
        String genre = null;
        switch (choice) {
            case 1:
                genre = "_TIKTOK";
                break;
            case 2:
                genre = "TIKTOK DANCE";
                break;
            case 3:
                genre = "TIKTOK OPM";
                break;
            case 4:
                genre = "TIKTOK PHILIPPINES";
                break;
        }
        System.out.println("\n\n============================================================");
        System.out.println("                   SONGS UNDER " + genre);
        System.out.println("============================================================");


        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] columns = line.split(",");
                if (columns[22].equalsIgnoreCase(genre)) {
                    System.out.println(columns[1] + " by " + columns[3]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        backToMenu();
    }


    public void correlation(int column, String variableX) {
        String filePath = "src/res/New File.csv";
        double totalPopularity = 0.0;
        double totalX = 0.0;
        double product = 0.0;
        double sqPopularity = 0.0;
        double sqX = 0.0;
        int numberOfElements = 0;




        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            br.readLine();  // Skip the first line
            while ((line = br.readLine()) != null) {
                numberOfElements++;
                String[] col;
                if (!line.contains("\"")) {
                    col = line.split(",");
                } else {
                    col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); // Ignoring commas inside quotation marks
                }
                try {
                    double popularity = Double.parseDouble(col[7]);
                    double x = Double.parseDouble(col[column]);
                    totalPopularity += popularity;
                    totalX += x;
                    product += popularity * x;
                    sqPopularity += Math.pow(popularity, 2);
                    sqX += Math.pow(x, 2);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        double correlation = ((numberOfElements * product) - (totalX * totalPopularity)) /
                (Math.sqrt(((numberOfElements * sqX) - Math.pow(totalX, 2)) *
                        ((numberOfElements * sqPopularity) - Math.pow(totalPopularity, 2))));
        System.out.println("Correlation: " + correlation);
        if (correlation >= -1 && correlation < -0.7) {
            System.out.println("Very Strong Negative");
            System.out.println("As " + variableX + " increases, popularity significantly decreases.");
        } else if (correlation >= -0.7 && correlation < -0.5) {
            System.out.println("Strong Negative");
            System.out.println("As " + variableX + " increases, popularity tends to decrease, but the relationship may not be perfect.");
        } else if (correlation >= -0.5 && correlation < -0.3) {
            System.out.println("Moderate Negative");
            System.out.println("As " + variableX + " increases, popularity tends to decrease, but the relationship is not extremely strong.");
        } else if (correlation >= -0.3 && correlation < 0) {
            System.out.println("Weak Negative");
            System.out.println("As " + variableX + " increases, popularity tends to decrease, but the relationship is not very strong.");
        } else if (correlation == 0) {
            System.out.println("Weak or No Correlation");
            System.out.println("Changes in " + variableX + " do not predict changes in popularity.");
        } else if (correlation > 0 && correlation <= 0.3) {
            System.out.println("Weak Positive");
            System.out.println("There is a mild tendency that " + variableX + " increases together with popularity, but the relationship is not very strong.");
        } else if (correlation > 0.3 && correlation <= 0.5) {
            System.out.println("Moderate Positive");
            System.out.println("There is a tendency that " + variableX + " moves together with popularity, but the relationship is not extremely strong.");
        } else if (correlation > 0.5 && correlation <= 0.7) {
            System.out.println("Strong Positive");
            System.out.println("As " + variableX + " increases, popularity tends to increase, but the relationship may not be perfect.");
        } else if (correlation > 0.7 && correlation <= 1) {
            System.out.println("Very Strong Positive");
            System.out.println("As " + variableX + " increases, popularity significantly increases.");
        } else {
            System.out.println("Invalid correlation value");
        }
    }



    /**
     * Calculates and prints the average value of a specific element for songs by a given choice (e.g., artist)
     * @param choice
     * @param colNum
     * @param element
     */
    public void average(String choice, int colNum, String element) {
        Scanner scanner = new Scanner(System.in);
        String filePath = "src/res/9443 Dataset.csv";


        double totalX = 0;
        int numberOfSongs = 0;


        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] col;
                if (!line.contains("\"")) {
                    col = line.split(",");
                } else {
                    col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); // ignoring commas inside quotation marks
                }
                if (col[3].equalsIgnoreCase(choice)) {
                    numberOfSongs++;
                    try {
                        double x = Double.parseDouble(col[colNum]);
                        totalX += x;
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (numberOfSongs > 0) {
            double average = totalX / numberOfSongs;
            System.out.println("\n\n======================================================");
            System.out.println("       AVERAGE " + element + " OF SONGS BY " + choice);
            System.out.println("======================================================");
            System.out.println("            " + average);
        } else {
            System.out.println("No songs found for the selected artist.");
        }
    }


    public void loudnessAnalysis() {
        Scanner scanner = new Scanner(System.in);
        String filePath = "src/res/9443 Dataset.csv";








        System.out.println("\n\n=====================================================================================================================================");
        System.out.println("                                                     AVERAGE LOUDNESS OF SONGS                                                       ");
        System.out.println("                                                 Please pick an artist of your choice                                                ");
        System.out.println("=====================================================================================================================================");
        artists();
        System.out.println("-------------------------------------------------------------------------------------------------------------------------------------");
        System.out.print(" Input Choice: ");
        String choice = scanner.nextLine();
        double totalLoudness = 0;
        int numberOfSongs = 0;




        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            br.readLine();
            while ((line = br.readLine()) != null) {
                String[] col;
                if (!line.contains("\"")) {
                    col = line.split(",");
                } else {
                    col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); // ignoring commas inside quotation marks
                }
                if (col[3].equalsIgnoreCase(choice)) {
                    numberOfSongs++;
                    try {
                        double loudness = Double.parseDouble(col[11]);
                        totalLoudness += loudness;
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (numberOfSongs > 0) {
            double loudnessAve = totalLoudness / numberOfSongs;
            System.out.println("\n\n======================================================");
            System.out.println("       AVERAGE LOUDNESS OF SONGS BY " + choice);
            System.out.println("======================================================");
            System.out.println("            " + loudnessAve);
        } else {
            System.out.println("No songs found for the selected artist.");
        }
        backToMenu();
    }





    /**
     * Reads the dataset from a CSV file and creates a list of InformationRequirements objects.
     *
     * @param csvFile The path to the CSV file containing the dataset.
     * @return A list of InformationRequirements objects representing the dataset.
     */
    private static List<InformationRequirements> readDataset(String csvFile) {
        List<InformationRequirements> dataset = new ArrayList<>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(csvFile));
            String header = reader.readLine();
            String line;
            while ((line = reader.readLine()) != null) {
                String[] col;
                if (!line.contains("\"")) {
                    col = line.split(",");
                } else {
                    col = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); //ignoring commas inside quotation marks
                }
                // Check if the array has the expected number of elements
                if (col.length >= 23) {
                    InformationRequirements information = new InformationRequirements(
                            col[1], // Track Name
                            col[3], // Artist Name
                            col[4], // Album Name
                            col[6], // Release Date
                            col[7], // Popularity
                            col[22]// Genre
                    );
                    dataset.add(information);
                } else {
                    // Log a warning or handle the case where the data is not as expected
                    System.out.println("Warning: Invalid data in CSV file. Skipping line.");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dataset;
    } // End of readDataSet method





    public void frequency(int columnIndexToStore){
        String[] values;
        try {
            values = Files.lines(Paths.get(modifiedCsvFile))
                    .map(line -> line.split(","))
                    .filter(split -> split.length > columnIndexToStore)
                    .map(split -> split[columnIndexToStore])
                    .toArray(String[]::new);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.print("What Frequency Do You Want To Find?: ");
        String searchElement = sc.next();
        int count = Collections.frequency(List.of(values), searchElement);
        System.out.println(searchElement + " appeared " + count + " times");


        backToMenu();
    }


    public void percentageSolving(int columnIndex) {
        System.out.println("What do you want to look for?: ");
        sc.useDelimiter("\\n");
        String valueToLookFor = sc.next();

        try (BufferedReader br = new BufferedReader(new FileReader(modifiedCsvFile))) {
            String line;
            List<String> columnValues = new ArrayList<>();

            // Read the CSV file
            while ((line = br.readLine()) != null) {
                String[] values; // Assuming CSV values are comma-separated
                if (!line.contains("\"")) {
                    values = line.split(",");
                } else {
                    values = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", -1); //ignoring commas inside quotation marks
                }
                if (values.length > columnIndex) {
                    columnValues.add(values[columnIndex].trim());
                    System.out.println(Arrays.toString(values)); // Print values for debugging
                }
            }

            int totalCount = columnValues.size();
            int frequency = Collections.frequency(columnValues, valueToLookFor);
            double percentage = ((double) frequency * 100) / totalCount;
            System.out.println("Value: " + valueToLookFor + ", Percentage: " + percentage + "%");
            backToMenu();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * This method displays the danceability from LEAST TO MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingDanceability() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getDanceability))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Danceability: " + forMusic.getDanceability() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);




        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the danceability from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingDanceability() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getDanceability).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Danceability: " + forMusic.getDanceability() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
    }//end of method




    /**
     * This method displays the instrumentalness from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingInstrumentalness() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getInstrumentalness).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Instrumentalness: " + forMusic.getInstrumentalness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the instrumentalness from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void ascendingInstrumentalness() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getInstrumentalness))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Instrumentalness: " + forMusic.getInstrumentalness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);




        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the SPEECHINESS from LEAST TO MOST
     *
     * @throws IOException
     */
    public static void ascendingSpeechiness() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getSpeechiness))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Speechiness: " + forMusic.getSpeechiness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the SPEECHINESS from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingSpeechiness() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getSpeechiness).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Speechiness: " + forMusic.getSpeechiness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);




        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the ENERGY from LEAST TO MOST
     *
     * @throws IOException
     */
    public static void ascendingEnergy() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getEnergy))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Energy: " + forMusic.getEnergy() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the ENERGY from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingEnergy() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getEnergy).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Energy: " + forMusic.getEnergy() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the tracks in MAJOR KEY (1)
     *
     * @throws IOException
     */
    public static void majorKeyTrack() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .filter(informationRequirements -> "1".equalsIgnoreCase(informationRequirements.getKey()))//searches the objects
                .sorted(Comparator.comparing(InformationRequirements::getTrackName).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Key: " + forMusic.getKey() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the tracks in MINOR KEY (0)
     *
     * @throws IOException
     */
    public static void minorKeyTrack() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .filter(informationRequirements -> "0".equalsIgnoreCase(informationRequirements.getKey()))//searches the objects
                .sorted(Comparator.comparing(InformationRequirements::getTrackName).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Key: " + forMusic.getKey() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the tracks and their VOLUME from LOUDEST TO LOWER SOUND VOLUMES
     *
     * @throws IOException
     */
    public static void descendingLoudness() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getLoudness).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Loudness: " + forMusic.getLoudness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the tracks and their VOLUME from LOWER SOUND VOLUMES TO LOUDEST
     *
     * @throws IOException
     */
    public static void ascendingLoudness() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getLoudness))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Loudness: " + forMusic.getLoudness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    } // end of method




    /**
     * This method displays the ACOUSTIC from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingAcoustic() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getAcousticness).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Acousticness: " + forMusic.getAcousticness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the ACOUSTICNESS from LEAST TO MOST
     *
     * @throws IOException
     */
    public static void ascendingAcoustic() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getAcousticness))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Acousticness: " + forMusic.getAcousticness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays LIVELINESS from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingLively() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getLiveness).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Liveness: " + forMusic.getLiveness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the LIVELINESS from LEAST to MOST
     *
     * @throws IOException
     */
    public static void ascendingLively() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getLiveness))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Liveness: " + forMusic.getLiveness() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the tracks and their VALENCE from MOST TO LEAST
     *
     * @throws IOException
     */
    public static void descendingValence() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getValence).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Valence: " + forMusic.getValence() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the tracks and their LIVELINESS from LEAST to MOST
     *
     * @throws IOException
     */
    public static void ascendingValence() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getLiveness))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Valence: " + forMusic.getValence() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the tracks and their TEMPO from LEAST to MOST
     *
     * @throws IOException
     */
    public static void ascendingTempo() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getTempo))//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Tempo: " + forMusic.getTempo() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    /**
     * This method displays the tracks and their TEMPO from MOST to LEAST
     *
     * @throws IOException
     */
    public static void descendingTempo() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .sorted(Comparator.comparing(InformationRequirements::getTempo).reversed())//sorts the objects
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Tempo: " + forMusic.getTempo() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the track ID from LEAST TO MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingTrackID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getTrackID))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Track ID: " + forMusic.getTrackID() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the track ID from MOST TO LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingTrackID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getTrackID).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Track ID: " + forMusic.getTrackID() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the track name from LEAST TO MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingTrackName() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getTrackName))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the track name from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingTrackName() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getTrackName).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the artist ID from LEAST TO MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingArtistID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getArtistID))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Arist ID: " + forMusic.getArtistID() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the artist ID from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingArtistID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getArtistID).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Arist ID: " + forMusic.getArtistID() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the artist name from LEAST TO MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingArtistName() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getArtistName))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Artist Name: " + forMusic.getArtistName() + " || Artist ID: " + forMusic.getArtistID() + " || Track name: " + forMusic.getTrackName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the artist name from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingArtistName() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getArtistName).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Artist Name: " + forMusic.getArtistName() + " || Artist ID: " + forMusic.getArtistID() + " || Track name: " + forMusic.getTrackName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the artist name from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingAlbumID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getAlbumID))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Album ID: " + forMusic.getArtistName() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the artist name from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingAlbumID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getAlbumID).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Album ID: " + forMusic.getArtistName() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the duration from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingDuration() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getDurationMilliSecs))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Duration: " + forMusic.getDurationMilliSecs() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the duration from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingDuration() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getDurationMilliSecs).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Duration: " + forMusic.getDurationMilliSecs() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the release Date from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingReleaseDate() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getReleaseDate))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Release Date: " + forMusic.getReleaseDate() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the release Date from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingReleaseDate() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getReleaseDate).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Release Date: " + forMusic.getReleaseDate() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the popularity from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingPopularity() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getPopularity))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Popularity: " + forMusic.getPopularity() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the popularity from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingPopularity() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getPopularity).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Popularity: " + forMusic.getPopularity() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the playlist ID from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingPlaylistID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getPlaylistID))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Playlist ID: " + forMusic.getPlaylistID() + " || Playlist Name: " + forMusic.getPlaylistName() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the playlist ID from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingPlaylistID() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getPlaylistID).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Playlist ID: " + forMusic.getPlaylistID() + " || Playlist Name: " + forMusic.getPlaylistName() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the playlist ID from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingPlaylistName() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getPlaylistName))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Playlist Name: " + forMusic.getPlaylistName() + " || Playlist ID: " + forMusic.getPlaylistID() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the playlist ID from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingPlaylistName() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getPlaylistName).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Playlist Name: " + forMusic.getPlaylistName() + " || Playlist ID: " + forMusic.getPlaylistID();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the duration (minutes) from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingDurationMins() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getDurationMins))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Duration (Minutes): " + forMusic.getDurationMins() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the duration (minutes) from MOST to LEAST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingDurationMins() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getDurationMins).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Duration (Minutes): " + forMusic.getDurationMins() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the genre from LEAST to MOST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void ascendingGenre() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getGenre))
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Genre: " + forMusic.getGenre() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method


    /**
     * This method displays the genre from MOST to LEST
     * It also counts the number of tracks and prints the count.
     *
     * @throws IOException
     */
    public static void descendingGenre() throws IOException {
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0);
        Files.lines(csvFile)
                .distinct()
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .sorted(Comparator.comparing(InformationRequirements::getGenre).reversed())
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + "Genre: " + forMusic.getGenre() + " || Track name: " + forMusic.getTrackName() + " || Artist Name: " + forMusic.getArtistName();
                })
                .forEach(System.out::println);
        System.out.println("\nTotal items: " + itemCount.get());
    }//end of method




    //METHODS USED FOR SEARCHING//


    /**
     * This method reads the lines of the file as a stream, and searches the streams to include only the objects where the TRACK NAME is found
     *
     * @param findItem
     * @throws IOException
     */
    public static void searchTrackName(String findItem) throws IOException {
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .filter(informationRequirements -> findItem.equalsIgnoreCase(informationRequirements.getTrackName()))//searches the objects
                .forEach(forMusic -> System.out.println(forMusic.getTrackName() + " || Artist: " + forMusic.getArtistName() + " || Album ID: " + forMusic.getAlbumID() + " || Genre: " + forMusic.getGenre()));//displays the filtered and sorted fields




    }//end of method




    /**
     * This method reads the lines of the file as a stream, and searches the streams to include only the objects where the ARTIST NAME is found
     *
     * @param findItem
     * @throws IOException
     */
    public static void searchArtistName(String findItem) throws IOException {
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .filter(informationRequirements -> findItem.equalsIgnoreCase(informationRequirements.getArtistName())) //searches the objects
                .forEach(forMusic -> System.out.println(forMusic.getArtistName() + " || Track: " + forMusic.getTrackName() + " || Album ID" + forMusic.getAlbumID() + " || Genre: " + forMusic.getGenre()));//displays the filtered and sorted fields
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and searches the streams to include only the objects where the PLAYLIST NAME is found
     *
     * @param findItem
     * @throws IOException
     */
    public static void searchPlaylistName(String findItem) throws IOException {
        Files.lines(csvFile)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .filter(informationRequirements -> findItem.equalsIgnoreCase(informationRequirements.getPlaylistName()))//searches the objects
                .forEach(forMusic -> System.out.println(forMusic.getPlaylistName() + " || Playlist ID: " + forMusic.getPlaylistID()));//displays the filtered and sorted fields
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and searches the streams to include only the objects where the GENRE is found
     *
     * @param findItem
     * @return
     * @throws IOException
     */
    public static void searchGenre(String findItem) throws IOException {
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .filter(informationRequirements -> findItem.equalsIgnoreCase(informationRequirements.getGenre()))//searches the objects
                .forEach(forMusic -> System.out.println(forMusic.getTrackName() + " || Artist: " + forMusic.getArtistName()));//displays the filtered and sorted fields


    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique genres with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterGenre() throws IOException {
        System.out.println("Category: Genre\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getGenre();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique track id's with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterTrackId() throws IOException {
        System.out.println("Category: Track ID\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getTrackID();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique track names with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterTrackName() throws IOException {
        System.out.println("Category: Track Name\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getTrackName();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique artist ID's with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterArtistID() throws IOException {
        System.out.println("Category: Artist ID\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getArtistID();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique track names with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterArtistName() throws IOException {
        System.out.println("Category: Artist Name\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getArtistName();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique album IDs with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterAlbumID() throws IOException {
        System.out.println("Category: Album ID\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getAlbumID();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique duration (milliseconds) with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterDuration() throws IOException {
        System.out.println("Category: Duration\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getDurationMilliSecs();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique release dates with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterReleaseDates() throws IOException {
        System.out.println("Category: Release Dates\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getReleaseDate();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique popularity rates with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterPopularity() throws IOException {
        System.out.println("Category: Popularity\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getPopularity();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique dancability with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterDanceability() throws IOException {
        System.out.println("Category: Danceability\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getDanceability();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique energies with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterEnergy() throws IOException {
        System.out.println("Category: Energy\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getEnergy();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique keys with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterKey() throws IOException {
        System.out.println("Category: Key\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items
        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getKey();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique loudness with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterLoudness() throws IOException {
        System.out.println("Category: Loudness\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items
        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getLoudness();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique modes with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterMode() throws IOException {
        System.out.println("Category: Mode\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items
        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getMode();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique spechiness with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterSpeechiness() throws IOException {
        System.out.println("Category: Speechiness\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getSpeechiness();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique acousticness with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterAcousticness() throws IOException {
        System.out.println("Category: Acousticness\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getAcousticness();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique loudness with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterInstrumentalness() throws IOException {
        System.out.println("Category: Instrumentalness\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getInstrumentalness();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique liveness with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterLiveness() throws IOException {
        System.out.println("Category: Liveness\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getLiveness();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique valence with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterValence() throws IOException {
        System.out.println("Category: Loudness\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getValence();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique tempos with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterTempo() throws IOException {
        System.out.println("Category: Tempo\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getTempo();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique playlist IDs with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterPlaylistIDs() throws IOException {
        System.out.println("Category: Playlist ID\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getPlaylistID();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique Playlist names with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterPlaylistName() throws IOException {
        System.out.println("Category: Playlist Name\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getPlaylistName();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method




    /**
     * This method reads the lines of the file as a stream, and displays the unique duration (minutes) with numbering.
     * It also counts the number of genres and prints the count.
     *
     * @throws IOException
     */
    public static void filterDurationMins() throws IOException {
        System.out.println("Category: Duration (Minutes)\n");
        AtomicInteger lineNumber = new AtomicInteger(1); // Using AtomicInteger to ensure thread safety
        AtomicInteger itemCount = new AtomicInteger(0); // counts the number of items




        Files.lines(csvFile)
                .skip(1)//skips the first item (category name) in the csv file
                .map(JanDaleTester::getInfoReq)//converts every line of the csv file into an object
                .map(forMusic -> {
                    itemCount.incrementAndGet(); // Increment the counter for each item
                    return lineNumber.getAndIncrement() + ". " + forMusic.getDurationMins();//adds the line number to each item
                })
                .forEach(System.out::println);//displays the items
        System.out.println("\nTotal items: " + itemCount.get());//shows the number of items
    }//end of method


    /**
     * This method displays the oldest and newest songs based on release date.
     *
     * @throws IOException
     */
    public static void getOldestAndNewestSongs() throws IOException {
        System.out.println("OLDEST AND NEWEST SONGS\n");
        // Find the oldest song
        System.out.println("Oldest Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getReleaseDate))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Release Date: " + oldestSong.getReleaseDate()));


        // Find the newest song
        System.out.println("\nNewest Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getReleaseDate))
                .ifPresent(newestSong -> System.out.println("Track: " + newestSong.getTrackName() +
                        " || Release Date: " + newestSong.getReleaseDate()));
    }//end of method


    /**
     * This method displays the quietest and loudest songs based on loudness.
     *
     * @throws IOException
     */
    public static void getQuietestAndLoudestSongs() throws IOException {
        System.out.println("QUIETEST AND LOUDEST SONGS\n");
        // Find the oldest song
        System.out.println("Quietest Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getLoudness))
                .ifPresent(quietestSong -> System.out.println("Track: " + quietestSong.getTrackName() +
                        " || Loudness: " + quietestSong.getLoudness()));


        // Find the newest song
        System.out.println("\nLoudest Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getLoudness))
                .ifPresent(loudestSong -> System.out.println("Track: " + loudestSong.getTrackName() +
                        " || Loudness: " + loudestSong.getLoudness()));
    }//end of method


    /**
     * This method displays the shortest and longest songs based on duration.
     *
     * @throws IOException
     */
    public static void getShortestAndLongestSongs() throws IOException {
        System.out.println("SHORTEST AND LONGEST SONGS\n");
        // Find the oldest song
        System.out.println("Shortest Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getDurationMilliSecs))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Duration: " + oldestSong.getDurationMilliSecs() + " || Duration (Minutes): " + oldestSong.getDurationMins()));


        // Find the newest song
        System.out.println("\nLongest Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getDurationMilliSecs))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Duration: " + oldestSong.getDurationMilliSecs() + " || Duration (Minutes): " + oldestSong.getDurationMins()));
    }//end of method


    /**
     * This method displays the most and least popular songs based on popularity.
     *
     * @throws IOException
     */
    public static void getMostAndLeastPopularSongs() throws IOException {
        System.out.println("MOST AND LEAST POPULAR SONGS\n");
        // Find the oldest song
        System.out.println("Least Popular Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getPopularity))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Popularity: " + oldestSong.getPopularity()));


        // Find the newest song
        System.out.println("\nMost Popular Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getPopularity))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Popularity: " + oldestSong.getPopularity()));
    }//end of method


    /**
     * This method displays the most and least danceable songs based on danceability.
     *
     * @throws IOException
     */
    public static void getMostAndLeastDanceableSongs() throws IOException {
        System.out.println("MOST AND LEAST DANCEABLE SONGS\n");
        // Find the oldest song
        System.out.println("Least Danceable Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getDanceability))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Danceability: " + oldestSong.getDanceability()));


        // Find the newest song
        System.out.println("\nMost Danceable Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getDanceability))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Danceability: " + oldestSong.getDanceability()));
    }//end of method


    /**
     * This method displays the most and least energetic songs based on energy.
     *
     * @throws IOException
     */
    public static void getMostAndLeastEnergeticSongs() throws IOException {
        System.out.println("MOST AND LEAST ENERGETIC SONGS\n");
        // Find the oldest song
        System.out.println("Least Energetic Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getEnergy))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Energy: " + oldestSong.getEnergy()));


        // Find the newest song
        System.out.println("\nMost Energetic Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getEnergy))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Energy: " + oldestSong.getEnergy()));
    }//end of method


    /**
     * This method displays the most and least lively songs based on liveness.
     *
     * @throws IOException
     */
    public static void getMostAndLeastLivelySongs() throws IOException {
        System.out.println("MOST AND LEAST LIVELY SONGS\n");
        // Find the oldest song
        System.out.println("Least Lively Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getLiveness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Liveness: " + oldestSong.getLiveness()));


        // Find the newest song
        System.out.println("\nMost Lively Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getLiveness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Liveness: " + oldestSong.getLiveness()));
    }//end of method


    /**
     * This method displays the highest and lowest valence songs based on valence.
     *
     * @throws IOException
     */
    public static void getHighestAndLowestValenceSongs() throws IOException {
        System.out.println("HIGHEST AND LOWEST VALENCE SONGS\n");
        // Find the oldest song
        System.out.println("Lowest Valence Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getValence))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Valence: " + oldestSong.getValence()));


        // Find the newest song
        System.out.println("\nHighest Valence Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getValence))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Valence: " + oldestSong.getValence()));
    }//end of method


    /**
     * This method displays the highest and lowest tempo songs based on tempo.
     *
     * @throws IOException
     */
    public static void getHighestAndLowestTempoSongs() throws IOException {
        System.out.println("HIGHEST AND LOWEST TEMPO SONGS\n");
        // Find the oldest song
        System.out.println("Lowest Tempo Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getTempo))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Tempo: " + oldestSong.getTempo()));


        // Find the newest song
        System.out.println("\nHighest Tempo Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getTempo))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Tempo: " + oldestSong.getTempo()));
    }//end of method


    /**
     * This method displays the most and least instrumental songs based on instrumentalness.
     *
     * @throws IOException
     */
    public static void getMostAndLeastInstrumentalSongs() throws IOException {
        System.out.println("MOST AND LEAST INSTRUMENTAL SONGS\n");
        // Find the oldest song
        System.out.println("Least Instrumental Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getInstrumentalness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Instrumentalness: " + oldestSong.getInstrumentalness()));


        // Find the newest song
        System.out.println("\nMost Lively Song:");
        Files.lines(csvFile)
                .skip(1)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getInstrumentalness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Instrumentalness: " + oldestSong.getInstrumentalness()));
    }//end of method


    /**
     * This method displays the most and least acoustic songs based on acousticness.
     *
     * @throws IOException
     */
    public static void getMostAndLeastAcousticSongs() throws IOException {
        System.out.println("MOST AND LEAST ACOUSTIC SONGS\n");
        // Find the oldest song
        System.out.println("Least Acoustic Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getAcousticness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Acousticness: " + oldestSong.getAcousticness()));


        // Find the newest song
        System.out.println("\nMost Lively Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getAcousticness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Acousticness: " + oldestSong.getAcousticness()));
    }//end of method


    /**
     * This method displays the most and least speechy songs based on speechiness.
     *
     * @throws IOException
     */
    public static void getMostAndLeastSpeechinessSongs() throws IOException {
        System.out.println("MOST AND LEAST SPEECHINESS SONGS\n");
        // Find the oldest song
        System.out.println("Least Speechy Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .min(Comparator.comparing(InformationRequirements::getSpeechiness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Speechinesss: " + oldestSong.getSpeechiness()));


        // Find the newest song
        System.out.println("\nMost Lively Song:");
        Files.lines(csvFile)
                .skip(1)
                .map(JanDaleTester::getInfoReq)
                .max(Comparator.comparing(InformationRequirements::getSpeechiness))
                .ifPresent(oldestSong -> System.out.println("Track: " + oldestSong.getTrackName() +
                        " || Speechinesss: " + oldestSong.getSpeechiness()));
    }//end of method


    public static void searchItem(String findItem) throws IOException {
        System.out.println("Searching for: " + findItem + "\n");
        searchGenre(findItem);
        searchGenre(findItem);
        searchArtistName(findItem);
        searchPlaylistName(findItem);
        searchPlaylistName(findItem);
        searchTrackName(findItem);
    }//end of method

    /**
     * Allows the user to select a category to filter and sort. Provides options to choose from, such as track ID,
     *  * track name, artist ID, artist name, and more. The user is prompted to enter a number between 1-23 based on their
     *  * selection. The method then allows sorting in ascending or descending order based on the chosen category.
     * @param scanner
     * @throws IOException
     */
    public static void sortItem(Scanner scanner) throws IOException {
        int sortNumber = 0;
        int sortOrder = 0;
        do{
            System.out.println("Select what to filter: 1. Track ID\n" +
                    "2. Track name\n" +
                    "3. Artist ID\n" +
                    "4. Artist name\n" +
                    "5. Album ID\n" +
                    "6. Duration\n" +
                    "7. Release date\n" +
                    "8. Popularity\n" +
                    "9. Danceability\n" +
                    "10. Energy\n" +
                    "11. Key\n" +
                    "12. Loudness\n" +
                    "14. Speechiness\n" +
                    "15. Acousticness\n" +
                    "16. Instrumentalness\n" +
                    "17. Liveness\n" +
                    "18. Valence\n" +
                    "19. Tempo\n" +
                    "20. Playlist ID\n" +
                    "21. Playlist name\n" +
                    "22. Duration mins\n" +
                    "23. Genre\n");
            sortNumber = scanner.nextInt();
            if (sortNumber != 1 && sortNumber != 2 && sortNumber != 3 && sortNumber != 4 && sortNumber != 5 && sortNumber != 6
                    && sortNumber != 7 && sortNumber != 8 && sortNumber != 9 && sortNumber != 10&& sortNumber != 11&& sortNumber != 12
                    && sortNumber != 14&& sortNumber != 15&& sortNumber != 16&& sortNumber != 17&& sortNumber != 18
                    && sortNumber != 19&& sortNumber != 20&& sortNumber != 21&& sortNumber != 22&& sortNumber != 23)
                System.out.println("Enter a number between 1-23!");
        }while(sortNumber != 1 && sortNumber != 2 && sortNumber != 3 && sortNumber != 4 && sortNumber != 5 && sortNumber != 6
                && sortNumber != 7 && sortNumber != 8 && sortNumber != 9 && sortNumber != 10&& sortNumber != 11&& sortNumber != 12
                && sortNumber != 14&& sortNumber != 15&& sortNumber != 16&& sortNumber != 17&& sortNumber != 18
                && sortNumber != 19&& sortNumber != 20&& sortNumber != 21&& sortNumber != 22&& sortNumber != 23);


        switch (sortNumber) {
            case 1:
                System.out.println("TRACK NAME" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingTrackID();
                        break;
                    case 2:
                        descendingTrackID();
                        break;
                }
                break;
            case 2:
                System.out.println("TRACK NAME" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingArtistName();
                        break;
                    case 2:
                        descendingArtistName();
                        break;
                }
                break;
            case 3:
                System.out.println("ARTIST ID" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingArtistID();
                        break;
                    case 2:
                        descendingArtistID();
                        break;
                }
                break;
            case 4:
                System.out.println("ARTIST NAME" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingArtistName();
                        break;
                    case 2:
                        descendingArtistName();
                        break;
                }
                break;
            case 5:
                System.out.println("ALBUM ID" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingAlbumID();
                        break;
                    case 2:
                        descendingAlbumID();
                        break;
                }
                break;
            case 6:
                System.out.println("DURATION" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingDuration();
                        break;
                    case 2:
                        descendingDuration();
                        break;
                }
                break;
            case 7:
                System.out.println("RELEASE DATE" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingReleaseDate();
                        break;
                    case 2:
                        descendingReleaseDate();
                        break;
                }
                break;
            case 8:
                System.out.println("POPULARITY" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingPopularity();
                        break;
                    case 2:
                        descendingPopularity();
                        break;
                }
                break;
            case 9:
                System.out.println("DANCEABILITY" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingDanceability();
                        break;
                    case 2:
                        descendingDanceability();
                        break;
                }
                break;
            case 10:
                System.out.println("ENERGY" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingEnergy();
                        break;
                    case 2:
                        descendingEnergy();
                        break;
                }
                break;
            case 11:
                System.out.println("KEY" +
                        "\n1. Major Key\n" +
                        "2. Minor Key");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        majorKeyTrack();
                        break;
                    case 2:
                        minorKeyTrack();
                        break;
                }
                break;
            case 12:
                System.out.println("LOUDNESS" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingLoudness();
                        break;
                    case 2:
                        descendingLoudness();
                        break;
                }
                break;
            case 14:
                System.out.println("SPEECHINESS" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingSpeechiness();
                        break;
                    case 2:
                        descendingSpeechiness();
                        break;
                }
                break;
            case 15:
                System.out.println("ACOUSTICNESS" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingAcoustic();
                        break;
                    case 2:
                        descendingAcoustic();
                        break;
                }
                break;
            case 16:
                System.out.println("ALBUM ID" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingInstrumentalness();
                        break;
                    case 2:
                        descendingInstrumentalness();
                        break;
                }
                break;
            case 17:
                System.out.println("LIVENESS" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingLively();
                        break;
                    case 2:
                        descendingLively();
                        break;
                }
                break;
            case 18:
                System.out.println("VALENCE" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingValence();
                        break;
                    case 2:
                        descendingValence();
                        break;
                }
                break;
            case 19:
                System.out.println("TEMPO" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingTempo();
                        break;
                    case 2:
                        descendingTempo();
                        break;
                }
                break;
            case 20:
                System.out.println("PLAYLIST ID" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingPlaylistID();
                        break;
                    case 2:
                        descendingPlaylistID();
                        break;
                }
                break;
            case 21:
                System.out.println("PLAYLIST NAME" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingPlaylistName();
                        break;
                    case 2:
                        descendingPlaylistName();
                        break;
                }
                break;
            case 22:
                System.out.println("DURATION MINUTES" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingDurationMins();
                        break;
                    case 2:
                        descendingDurationMins();
                        break;
                }
                break;
            case 23:
                System.out.println("GENRE" +
                        "\n1. Ascending\n" +
                        "2. Descending");
                sortOrder = scanner.nextInt();
                switch (sortOrder) {
                    case 1:
                        ascendingGenre();
                        break;
                    case 2:
                        descendingGenre();
                        break;
                }
                break;
        }//end of switch
    }//end of method



    /**
     * Allows the user to select a category to filter data. Displays options for filtering, such as track ID,
     * track name, artist ID, artist name, and more. The user is prompted to enter a number between 1-23 based on their
     * selection. The method then invokes specific filter methods based on the chosen category.
     * @param scanner
     * @throws IOException
     */
    public static void filterCategory(Scanner scanner) throws IOException {
        int filterNum = 0;
        do{
            System.out.println("Select what to filter: 1. Track ID\n" +
                    "2. Track name\n" +
                    "3. Artist ID\n" +
                    "4. Artist name\n" +
                    "5. Album ID\n" +
                    "6. Duration\n" +
                    "7. Release date\n" +
                    "8. Popularity\n" +
                    "9. Danceability\n" +
                    "10. Energy\n" +
                    "11. Key\n" +
                    "12. Loudness\n" +
                    "13. Mode\n" +
                    "14. Speechiness\n" +
                    "15. Acousticness\n" +
                    "16. Instrumentalness\n" +
                    "17. Liveness\n" +
                    "18. Valence\n" +
                    "19. Tempo\n" +
                    "20. Playlist ID\n" +
                    "21. Playlist name\n" +
                    "22. Duration mins\n" +
                    "23. Genre\n");
            filterNum = scanner.nextInt();
            if (filterNum != 1 && filterNum != 2 && filterNum != 3 && filterNum != 4 && filterNum != 5 && filterNum != 6
                    && filterNum != 7 && filterNum != 8 && filterNum != 9 && filterNum != 10&& filterNum != 11&& filterNum != 12
                    && filterNum != 13&& filterNum != 14&& filterNum != 15&& filterNum != 16&& filterNum != 17&& filterNum != 18
                    && filterNum != 19&& filterNum != 20&& filterNum != 21&& filterNum != 22&& filterNum != 23)
                System.out.println("Enter a number between 1-23!");
        }while(filterNum != 1 && filterNum != 2 && filterNum != 3 && filterNum != 4 && filterNum != 5 && filterNum != 6
                && filterNum != 7 && filterNum != 8 && filterNum != 9 && filterNum != 10&& filterNum != 11&& filterNum != 12
                && filterNum != 13&& filterNum != 14&& filterNum != 15&& filterNum != 16&& filterNum != 17&& filterNum != 18
                && filterNum != 19&& filterNum != 20&& filterNum != 21&& filterNum != 22&& filterNum != 23);


        switch (filterNum) {
            case 1:
                System.out.println("TRACK ID\n");
                filterTrackId();
                break;
            case 2:
                System.out.println("TRACK NAME\n");
                filterTrackName();
                break;
            case 3:
                System.out.println("ARTIST ID\n");
                filterArtistID();
                break;
            case 4:
                System.out.println("ARTIST NAME\n");
                filterArtistName();
                break;
            case 5:
                System.out.println("ALBUM ID\n");
                filterAlbumID();
                break;
            case 6:
                System.out.println("DURATION\n");
                filterDuration();
                break;
            case 7:
                System.out.println("RELEASE DATE\n");
                filterReleaseDates();
                break;
            case 8:
                System.out.println("POPULARITY\n");
                filterPopularity();
                break;
            case 9:
                System.out.println("DANCEABILITY\n");
                filterDanceability();
                break;
            case 10:
                System.out.println("ENERGY\n");
                filterEnergy();
                break;
            case 11:
                System.out.println("KEY\n");
                filterKey();
                break;
            case 12:
                System.out.println("LOUDNESS\n");
                filterLoudness();
                break;
            case 14:
                System.out.println("SPEECHINESS\n");
                filterSpeechiness();
                break;
            case 15:
                System.out.println("ACOUSTICNESS\n");
                filterAcousticness();
                break;
            case 16:
                System.out.println("INSTRUMENTALNESS\n");
                filterInstrumentalness();
                break;
            case 17:
                System.out.println("LIVENESS\n");
                filterLiveness();
                break;
            case 18:
                System.out.println("VALENCE\n");
                filterValence();
                break;
            case 19:
                System.out.println("TEMPO\n");
                filterTempo();
                break;
            case 20:
                System.out.println("PLAYLIST ID\n");
                filterPlaylistIDs();
                break;
            case 21:
                System.out.println("PLAYLIST NAME\n");
                filterPlaylistName();
                break;
            case 22:
                System.out.println("DURATION MINS\n");
                filterDurationMins();
                break;
            case 23:
                System.out.println("GENRE\n");
                filterGenre();
                break;
        }//end of switch
    }//end of method

    /**
     * Asks the user if they want to go back to the main menu.
     * If 'Y' is entered, returns to the main menu; if 'N' is entered, exits the program.
     */
    public void backToMenu() {
        boolean flag = false;
        do{
            System.out.println("Do you want to go back to the main menu? (Y/N): ");
            sc.useDelimiter("\\n");
            String response = sc.nextLine();
            if(!response.equalsIgnoreCase("Y") || !response.equalsIgnoreCase("N")){
                System.out.println("Please enter a valid response!\n");
            }
            if(response.equalsIgnoreCase("Y")){
                flag = true;
                System.out.println("\nRETURNING TO MENU...");
                menu();
            }
            if(response.equalsIgnoreCase("N")) {
                System.out.println("Thank you for using the program!");
                System.exit(0);
            }
        } while (!flag);
    } // End of backToMenu method



    /**
     * Retrieves and displays unique artist data from the modified CSV file.
     */
    public void uniqueData() {
        try {
            Files.lines(Path.of(modifiedCsvFile))
                    .distinct()
                    .skip(1)
                    .map(JanDaleTester::getInfoReq)
                    .map(forMusic -> {
                        return  " " + forMusic.getArtistName()+ " || Genre: "+ forMusic.getGenre();
                    })
                    .forEach(System.out::printf);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    } // End of uniqueData method


    /**
     * Lists distinct artist names from the modified CSV file.
     */
    public void artists() {
        try {
            Files.lines(Path.of(modifiedCsvFile))
                    .distinct()
                    .skip(1)
                    .map(JanDaleTester::getInfoReq)
                    .map(forMusic -> {
                        return  " "+ forMusic.getArtistName() + " ";
                    })
                    .forEach(x -> System.out.printf("%-10s%-10s" , x , " "));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println(" ");
    } // End of artists method
} // End of Activity0 class



